import aws from 'aws-sdk'
import crypto from 'crypto'
import { promisify } from "util"
const randomBytes = promisify(crypto.randomBytes)

//dotenv.config()

const region = "ap-southeast-2"
const bucketName = "ideate-blog-profile-images"
const accessKeyId = "AKIAQBRVKJDOOPHEXYGL"
const secretAccessKey = "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"
const s3 = new aws.S3({
  region,
  accessKeyId,
  secretAccessKey,
  signatureVersion: 'v4'
})

export async function generateUploadURL() {
  const rawBytes = await randomBytes(16)
  const imageName = rawBytes.toString('hex')

  const params = ({
    Bucket: bucketName,
    Key: imageName,
    Expires: 180
  })
  
  const uploadURL = await s3.getSignedUrlPromise('putObject', params)
  return uploadURL
}