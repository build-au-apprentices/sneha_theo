import express, { Request } from "express";
import cors from "cors";
import userRoutes from "../Routes/userRoutes";
import blogRoutes from "../Routes/blogRoutes";
import commentRoutes from "../Routes/commentRoutes"
import favoriteRoutes from "../Routes/favouriteRoutes"
import { Response } from "express-serve-static-core";
import { generateUploadURL } from './s3'

export const app = express();

app.use(express.json());

app.use(cors());

app.get('/s3Url', async (req, res) => {
  const url = await generateUploadURL()
  res.send({url})
})

app.use("/", userRoutes);
app.use("/blogs", blogRoutes);
app.use("/comments", commentRoutes);
app.use("/favorite", favoriteRoutes);

app.use(function (req, res, next) {
  res.status(404).json({
    message: "No such route exists",
  });
});

app.use(function (req: Request, res: Response) {
  res.status(500).json({
    message: "Error Message",
  });
});



app.listen(8080, () => console.log(`Server ready at: http://localhost:8080`));
