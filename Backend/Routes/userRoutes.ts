import express from 'express';
import userController from '../Controllers/userController';

const router = express.Router();

router.get('/userByUsername/:username', userController.getByUsername);
router.get('/userByName/:name', userController.usersByName);
router.get('/userByLastName/:lname', userController.usersByLastname);
router.post('/createUser',userController.createUser);
router.put('/updateUser/:id',userController.updateUser);
router.delete('/deleteUser/:id',userController.deleteUser);
router.get('/users', userController.allUsers);

export default router;