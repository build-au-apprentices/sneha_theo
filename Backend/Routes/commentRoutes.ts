import express from 'express';
import commentControllers from '../Controllers/commentControllers';

const router = express.Router();

router.get('/allComments/:id', commentControllers.blogComments);
router.post('/create',commentControllers.createComment);
router.put('/update/:id',commentControllers.updatComment);
router.delete('/delete/:id',commentControllers.deleteComment);


export default router;