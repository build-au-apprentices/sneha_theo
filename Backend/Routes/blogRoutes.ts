import express from 'express';
import blogControllers from '../Controllers/blogControllers';

const router = express.Router();

router.get('/allBlogs', blogControllers.allBlogs);
router.get('/byId/:id', blogControllers.byId);
router.get('/byUser/:id', blogControllers.blogsByUser);
router.post('/create',blogControllers.create);
router.put('/update/:id',blogControllers.update);
router.delete('/delete/:id',blogControllers.deleteBlog);
router.delete('/deleteAll/:id',blogControllers.deleteAll);
//router.get('/favourites/:userId', blogControllers.favourites);
router.get('/drafts/:userId', blogControllers.drafts);
router.get('/published', blogControllers.published);

export default router;