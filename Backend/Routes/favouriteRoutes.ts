import express from 'express';

import Controller from '../Controllers/favouriteControllers'

const router = express.Router();
// routes for accessing
router.get('/myFavorites/:id', Controller.getUserFavorites)
router.get('/getFavorite/:userId/:blogId', Controller.getFavorite)
router.post('/add', Controller.addFavoriteBlog)
router.delete('/remove', Controller.deleteFavorite)

export default router