-- DropIndex
DROP INDEX "comment_id";

-- AlterTable
ALTER TABLE "Comment" ADD COLUMN     "commentId" SERIAL NOT NULL,
ADD CONSTRAINT "Comment_pkey" PRIMARY KEY ("commentId");
