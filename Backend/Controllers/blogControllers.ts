import { PrismaClient } from "@prisma/client";
import { Request, Response } from "express";

const prisma = new PrismaClient();

const allBlogs = async (req: Request, res: Response) => {
  try {
    const blogs = await prisma.blog.findMany();
    res.json(blogs);
  } catch (e) {
    console.error(e);
  }
};

const published = async (req: Request, res: Response) => {
  try {
    const blogs = await prisma.blog.findMany({
      where: {
        isPublished: true,
      },
    });
    res.json(blogs);
  } catch (e) {
    console.error(e);
  }
};

const blogsByUser = async (req: Request, res: Response) => {
  try {
    const { id }: { id?: string } = req.params;
    const blogs = await prisma.blog.findMany({
      where: {
        userId: id,
        isPublished:true
      },
    });
    res.json(blogs);
  } catch (error) {
    console.error(error);
  }
};

// const favourites = async (req: Request, res: Response) => {
//   try {
//     const { userId }: { userId?: string } = req.params;
//     const blogs = await prisma.favourite.findMany({
//       where: {
//         userId: userId,
//       },
//     });
//     res.json(blogs);
//   } catch (error) {
//     console.error(error);
//   }
// };

const drafts = async (req: Request, res: Response) => {
  try {
    const { userId }: { userId?: string } = req.params;
    const blogs = await prisma.blog.findMany({
      where: {
        userId: userId,
        isPublished: false,
      },
    });
    res.json(blogs);
  } catch (error) {
    console.error(error);
  }
};

//create blog
const create = async (req: Request, res: Response) => {
  try {
    const { title, summary, userId, content, image, likes, isPublished } = req.body;
    const user = await prisma.user.findUnique({
      where: {
        userId: userId,
      },
    });
    if (user) {
      const result = await prisma.blog.create({
        data: {
          title,
          summary,
          userId,
          content,
          image,
          likes,
          isPublished
        },
      });
      res.json(result);
    } else {
      res.json("user doesn't exist!");
    }
  } catch (error) {
    console.error(error);
  }
};

//update blog
const update = async (req: Request, res: Response) => {
  try {
    const { id }: { id?: string } = req.params;
    const {
      title,
      summary,
      userId,
      content,
      image,
      likes,
      isPublished,
    } = req.body;
    const result = await prisma.blog.update({
      where: {
        blogId: Number(id),
      },
      data: {
        title,
        summary,
        userId,
        content,
        image,
        likes,
        isPublished,
      },
    });
    res.json(result);
  } catch (error) {
    console.error(error);
  }
};

//delete blog
const deleteBlog = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const result = await prisma.blog.delete({
      where: {
        blogId: Number(id),
      },
    });
    res.json(result);
  } catch (error) {
    console.error(error);
  }
};

//blogbytitle
const byId = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const blogs = await prisma.blog.findUnique({
      where: {
        blogId: Number(id),
      },
    });
    res.json(blogs);
  } catch (error) {
    console.error(error);
  }
};

const deleteAll = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const result = await prisma.blog.deleteMany({
      where: {
        userId: id,
      },
    });
    res.json(result);
  } catch (error) {
    console.error(error);
  }
};

export default {
  blogsByUser,
  byId,
  update,
  drafts,
  create,
  deleteBlog,
  allBlogs,
  deleteAll,
  published
};
