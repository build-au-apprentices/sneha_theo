import { PrismaClient, Prisma } from '@prisma/client';
import { Request, Response } from 'express';
const prisma = new PrismaClient();

// Get all Favorite blogs for a particular user
const getUserFavorites = async (req: Request, res: Response) => {
    try {
        // Get the ID of the user
        const { id } = req.params
        
        // Match the favorite blogs to the particular user
        const favoriteBlogs = await prisma.favorite.findMany({
            where:{
                userId: String(id)
            },
            select:{
                blog:true
            }
            
            
        })

        // Return the blogs that are found
        res.json(favoriteBlogs)
    }catch (error) {
        console.log(error)
    }
}

// Get a Single Favorite blog and user
const getFavorite = async (req: Request, res: Response) => {
    try {
        // Get the ID of the user
        const { userId, blogId } = req.params
        // Match the favorite blogs to the particular user
        const favoriteBlogs = await prisma.favorite.findFirst({
            where:{
                userId: userId,
                blogId: Number(blogId)
            }
        })

        // Return the blogs that are found
        res.json(favoriteBlogs)
    }catch (error) {
        console.log(error)
    }
}

// Add a Favorite Blog for a particular User
const addFavoriteBlog = async (req: Request, res: Response) => {
    try {
        // Need a userId and blogId to favorite a particular blog
        const { userId, blogId} = req.body
        const response = await prisma.favorite.create({
            data: {
                userId,
                blogId
            }
        })

        // Response of the data that we added to confirm creation
        res.json(response)
    }catch (error){
        console.log(error)
    }
}

// Remove a favorite blog from a particular user 
const deleteFavorite = async (req: Request, res: Response) => {
    try {
        // Need a favoriteId to delete a blog
        const { userId, blogId } = req.body
        
        const response = await prisma.favorite.deleteMany({
            where: {
                userId: userId,
                blogId: Number(blogId)
            }
        })
        
        // Response of deletion
        res.json(response)
    }catch (error){
        console.log(error)
    }
}

// Export all functions
export default {
    getUserFavorites,
    getFavorite,
    addFavoriteBlog,
    deleteFavorite
}