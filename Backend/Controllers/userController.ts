import { PrismaClient, Prisma } from '@prisma/client';
import { Request, Response } from 'express';
import blogController from './blogControllers';
const prisma = new PrismaClient();
//export const app = express();

//app.use(express.json());



const allUsers = async (req: Request, res: Response) => {
    try {
        const users = await prisma.user.findMany()
        res.json(users)
    } catch (error) {
        console.error(error)
    }

};

const getByUsername = async (req: Request, res: Response) => {

    try {
        const { username }: { username?: string } = req.params
        //let {username} = req.body;
        const user = await prisma.user.findMany({
            where: {
                userId: username,
            }
        })
        res.json(user)
    } catch (error) {
        console.error(error)
    }

};

const createUser = async (req: Request, res: Response) => {

    try {
        const { userId, firstName, lastName, email } = req.body
        const result = await prisma.user.create({
            data: {
                userId,
                firstName,
                lastName,
                pronouns: "",
                email,
                profile_img: "",
                isAdmin: false
            }
        })
        res.json(result)
    } catch (error) {
        console.error(error)
    }

}

const updateUser = async (req: Request, res: Response) => {

    try {
        const { id }: { id?: string } = req.params
        const { firstName, lastName, pronouns, profile_img, date_mod, isAdmin } = req.body
        const result = await prisma.user.update({
            where: {
                userId: id,
            },
            data: {
                firstName,
                lastName,
                pronouns,
                profile_img,
                date_mod,
                isAdmin
            }
        })
        res.json(result)
    } catch (error) {
        console.error(error)
    }

};

const deleteUser = async (req: Request, res: Response) => {

    try {
        const { id } = req.params
        const result = await prisma.user.delete({
            where: {
                userId: id,
            }
        })
        res.json(result)
    } catch (error) {
        console.error(error)
    }

}

const usersByName = async (req: Request, res: Response) => {

    try {
        const { name }: { name?: string } = req.params
        const user = await prisma.user.findMany({
            where: {
                firstName: name,
            }
        })
        res.json(user)
    } catch (error) {
        console.error(error)
    }

};

const usersByLastname = async (req: Request, res: Response) => {

    try {
        const { lname }: { lname?: string } = req.params
        const user = await prisma.user.findMany({
            where: {
                lastName: lname,
            }
        })
        res.json(user)
    } catch (error) {
        console.error(error)
    }

};

export default {
    getByUsername,
    allUsers,
    usersByLastname,
    usersByName,
    updateUser,
    createUser,
    deleteUser,
};