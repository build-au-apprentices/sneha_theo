import { PrismaClient, Prisma } from '@prisma/client';
import { Request, Response } from 'express';
const prisma = new PrismaClient();
//export const app = express();

// get all the comments for blog
const blogComments = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const comments = await prisma.comment.findMany({
            where:{
                blogId: Number(id)
            }
        })
        res.json(comments)
    } catch (error) {
        console.error(error)
    }

};

const createComment =  async (req: Request, res: Response) => {

    try {
        const { blogId, userId, message } = req.body
        const result = await prisma.comment.create({
            data: {
                blogId,
                userId,
                message
            }
        })
        res.json(result)
    } catch (error) {
        console.error(error)
    }

}

const updatComment = async (req: Request, res: Response) => {

    try {
        const { id } = req.params;
        const { message } = req.body
        const result = await prisma.comment.update({
            where:{
                commentId: Number(id)
            },
            data: {
                message
            }
        })
        res.json(result)
    } catch (error) {
        console.error(error)
    }

}

const deleteComment = async (req: Request, res: Response) => {

    try {
        const { id } = req.params;
        const result = await prisma.comment.delete({
            where: {
              commentId: Number(id)
            }
        })
        res.json(result)
    } catch (error) {
        console.error(error)
    }

}

export default {
    blogComments,
    createComment,
    deleteComment,
    updatComment
}
