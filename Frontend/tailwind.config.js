module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      screens: {
        'sm'  : '640px',
        'md'  : '768px',
        'lg'  : '1024px',
        'xl'  : '1280px',
        '2xl' : '1536px',
        'displaySmall' : {'min' : '200px', 'max' : '1200px'},
        'adminDisplay' : {'min' : '200px', 'max' : '1450px'}
      },
      colors: {
        'slalomBlue': '#81e0f1'
      }
    },
  },
  plugins: [],
}