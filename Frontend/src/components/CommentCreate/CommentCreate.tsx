import React, { useState } from 'react'
import { CreateComment } from '../../api/comment'
import {Authenticate} from '../Cognito/Authenticate'

interface CommentCreateProps {
    blogId: number
}

export const CommentCreate: React.FC<CommentCreateProps> = (props): JSX.Element => {
    const [commentText, setCommentText] = useState('')
    const [placeholder, setPlaceholder] = useState('Comment')
    const user = Authenticate()

    const sendComment = () => {
        if(validate()){
            // Auth.currentAuthenticatedUser()
            // .then(data => {
            //     // Backend Post Request Here 
            //     console.log('user:', data.username)
            //     console.log('blogId:', props.blogId)
            //     console.log('comment:', commentText)
            // })
            // .catch(error => {
            //     console.log(error)
            // })
            CreateComment(props.blogId,user,commentText)
            window.location.reload();
        }else{
            setPlaceholder('Must fill comment field before sending')
        }
    }

    const validate = () => {
        if(commentText.length > 0){
            return true
        }
        return false
    }

  return (
    <div className='flex flex-col mt-1'>
        <textarea placeholder={placeholder} value={commentText} onChange={(e) => setCommentText(e.target.value)} className=' text-black text-[12px] px-[3px] w-[100%] h-[50px] resize-none border-[1px] border-black outline-none'></textarea>
        <button onClick={() => sendComment()} className='text-white text-[13px] bg-stone-800 hover:bg-stone-700 cursor-pointer w-[50%] rounded-l-md self-end mt-1'>Send</button>
    </div>
  )
}
