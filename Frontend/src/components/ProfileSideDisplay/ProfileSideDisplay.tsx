import {useState } from 'react'
import { User } from '../../models'
import { PencilIcon } from '@heroicons/react/outline'
import { Authenticate } from '../Cognito';
import { UpdateUser } from '../../api/user';
import React from 'react';

export const ProfileSideDisplay = ({user} : {user : User}) => {
    const [inputDisabled, setInputDisabled] = useState(true)
    const [firstName, setFirstName] = useState(user.firstName)
    const [lastName, setLastName] = useState(user.lastName)
    const [email, setEmail] = useState(user.email) // TODO Need to change this to preferred name
    const [pronouns, setPronouns] = useState(user.pronouns)
    let profile_img  = user.profile_img!=""? user.profile_img : "https://ideate-blog-profile-images.s3.ap-southeast-2.amazonaws.com/profile_default.png"
    const userId = Authenticate()
    const imageUploader = React.useRef<HTMLInputElement>(null);
    
    const onCancel = () => {
        setFirstName(user.firstName)
        setLastName(user.lastName)
        setEmail(user.email)
        setPronouns(user.pronouns)
        setInputDisabled(!inputDisabled)
    }

    const onSave = () => {
        setInputDisabled(!inputDisabled)
        UpdateUser(userId,firstName,lastName,pronouns)
    }


    const handleImageUpload = (event : React.FormEvent) => {
        const files = (event.target as HTMLInputElement).files
        
        if (files && files.length > 0) {
            console.log(files)
            uploadImage(files[0])
        }
       
    }

    const uploadImage = async (file:File) => {
        const { url } = await fetch("http://3.106.166.81:8080/s3Url").then(res => res.json())
        //console.log(url)
        
        try {
            await fetch(url, {
                method: "PUT",
                headers: {
                  "Content-Type": "multipart/form-data"
                },
                body: file
            }).then(()=>{
                profile_img = url.split('?')[0];
                console.log(profile_img + "img")
                UpdateUser(userId,firstName,lastName,pronouns,profile_img)
                window.location.reload()
            })
        } catch (error) {
            console.error(error)
        }
        //await axios.put(url,file)
        
    }

  return (
    <div className='flex mt-[25%] flex-col mx-4 my-2 text-white h-[100%] '>
        <input type="file" ref={imageUploader} accept="image/*" onChange={e=>handleImageUpload(e)} className="display:none" style={{display:'none'}} />
        <div title="Edit picture" className='flex flex-row self-center'>
            <button onClick={e => imageUploader?.current?.click()} className='w-[120px] h-[120px]'>
                <img className="object-cover mr-2 cursor-pointer rounded-full w-[120px] h-[120px]" src={profile_img} alt="File not found" onMouseOver={e => (e.currentTarget.src = "/edit_profile.jpeg")} onMouseLeave={e => (e.currentTarget.src =profile_img )}/>
            </button>
        </div>
        <div className='flex flex-col items-center content-center align-middle'>
            <h1 className='mt-6'>First Name:</h1>
            <input disabled={inputDisabled} onChange={e => setFirstName(e.target.value)} value={firstName} className='w-[70%] text-black rounded-md px-2 text-center disabled:bg-slate-200'></input>
            <h1 className='mt-4'>Last Name:</h1>
            <input disabled={inputDisabled} onChange={e => setLastName(e.target.value)} value={lastName} className='w-[70%] text-black rounded-md px-2 text-center disabled:bg-slate-200'></input>
            <h1 className='mt-4'>Pronouns</h1>
            <input disabled={inputDisabled} onChange={e => setPronouns(e.target.value)} value={pronouns} className='w-[70%] text-black rounded-md px-2 text-center disabled:bg-slate-200'></input>
            <h1 className='mt-4'>Email</h1>
            <p  className='w-[70%] text-black bg-slate-200  rounded-md px-2 text-center '>{email}</p>
        </div>
        <div className='flex flex-row self-end mt-6 mr-[15%]'>
            {inputDisabled ? <PencilIcon onClick={() => setInputDisabled(!inputDisabled)} className='cursor-pointer w-9 hover:text-slalomBlue'/> : null}
            {!inputDisabled ? <button onClick={() => onCancel()} data-testid='cancel-button' className='w-[65px] h-7 text-white bg-red-600 rounded-lg drop-shadow-md self-start hover:bg-red-400 mx-10'>Cancel</button> : null }
            {!inputDisabled ? <button onClick={() => onSave()} className='w-[65px] text-white rounded-lg cursor-pointer bg-sky-600 h-7 hover:bg-sky-500'>Save</button> : null}
            
        </div>
    </div>
  )
}
