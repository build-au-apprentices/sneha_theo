import React, { createRef } from "react";
import { Blog } from "../../models";
import LikedButtonInactive from "../../images/like-button-inactive.png";
import { TrashIcon, PencilIcon, StarIcon } from "@heroicons/react/outline";
import { NewBlogModal } from "../Modals/NewBlogModal";
import { ModalRefs } from "../../models/Modal";
import { BlogViewModal } from "../Modals/BlogViewModal";
import { DeleteBlog } from "../../api/blogs";
import { Authenticate } from "../Cognito/Authenticate";
import { DeleteFavorite } from "../../api/favorites";

export const UserBlogTabs = ({ blog , isFav, isDraft}: { blog: Blog , isFav?: boolean, isDraft?:boolean} ) => {
  const openEditBlogModalRef = createRef<ModalRefs>();
  const viewBlogModalRef = createRef<ModalRefs>();
  const user = Authenticate();
  const fav = isFav ? isFav : false

  const onDelete = () => {
    DeleteBlog(user, blog.blogId);
    window.location.reload();
  }

  const changeFavoriteState = () => {
    DeleteFavorite(user, blog.blogId)
    window.location.reload()  
  }
 

  const getIcons = () => {
    let toReturn
    if (!fav) {
      toReturn = 
      <div className="flex mt-2">
        <div className="flex gap-2 align-middle grow">

        </div>
        <PencilIcon
          onClick={() => openEditBlogModalRef.current?.openDialog()}
          className=" text-sky-300 hover:text-sky-500 w-[25px] cursor-pointer self-end mr-6"
        />
        {
          <NewBlogModal
            isNew={false}
            blog={blog}
            ref={openEditBlogModalRef}
          />
        }
        <TrashIcon onClick={() => onDelete()} className="text-red-700 w-[25px] cursor-pointer hover:text-red-500 self-end" />
      </div>
    }else{
      toReturn = 
      <div className="flex mt-2">
        <div className="flex gap-2 align-middle grow">
        </div>
        <StarIcon className={"cursor-pointer w-[25px] hover:text-white text-yellow-300"} onClick={() => changeFavoriteState()} />
      </div>
    }
    return (
      toReturn
    )
  }

  return (
    <div className="m-2 mx-[4%] text-left max-h-[200px]">
      <div className="flex flex-col">
        <div
          onClick={() => viewBlogModalRef.current?.openDialog()}
          className="flex flex-row cursor-pointer hover:bg-gray-900"
        >
          {<BlogViewModal blog={blog} isDraft={isDraft} ref={viewBlogModalRef} />}
          <div className="2xl:min-h-[160px] 2xl:min-w-[160px] mr-3 displaySmall:min-w-[125px] displaySmall:min-h-[125px] lg:min-w-[125px] lg:min-h-[125px]">
            <img
              className="object-cover 2xl:w-[160px] 2xl:h-[160px] displaySmall:w-[125px] displaySmall:h-[125px] lg:w-[125px] lg:h-[125px]"
              src={blog.image}
              alt="File not Found"
            />
          </div>

          <div className="w-[100%]">
            <div className="flex">
              <h1
                data-testid="title-display"
                className="overflow-hidden font-bold 2xl:text-2xl grow displaySmall:text-xl lg:text-xl text-ellipsis max-h-8"
              >
                {blog.title}
              </h1>
              <h1
                data-testid="date-display"
                className="text-right grow 2xl:text-sm displaySmall:text-xs lg:text-xs"
              >
                {String(blog.date_created).substring(0, 10)}
              </h1>
            </div>
            <div className="text-ellipsis overflow-hidden 2xl:max-h-[100px] displaySmall:max-h-[80px] lg:max-h-[82px] w-[100%]">
              <p
                data-testid="text-display"
                className="2xl:text-base displaySmall:text-sm lg:text-sm grow"
              >
                {blog.summary}
              </p>
            </div>
          </div>
        </div>

        <div className="">
          {/* <div className="invisible"> */}
          {/* <div className="flex gap-2 align-middle grow">

          </div> */}
          
          {getIcons()}
          {/* <PencilIcon
            onClick={() => openEditBlogModalRef.current?.openDialog()}
            className=" text-sky-300 hover:text-sky-500 w-[25px] cursor-pointer self-end mr-6 "
          />
          {
            <NewBlogModal
              isNew={false}
              blog={blog}
              ref={openEditBlogModalRef}
            />
          }
          <TrashIcon onClick={() => onDelete()} className="text-red-700 w-[25px] cursor-pointer hover:text-red-500 self-end" /> */}
        {/* </div> */}
        </div>
      </div>
      <hr className="border-1 border-slalomBlue mt-3 rounded-full w-[100%] text-center" />
    </div>
  );
};
