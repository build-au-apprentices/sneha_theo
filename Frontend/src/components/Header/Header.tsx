
import logo from '../../images/slalom-logo.png'
import { ReactComponent as Exit } from '../../images/logout-button.svg'
import { logout } from '../Cognito/Logout'
import { User } from '../../models'
import { GetUser } from '../../api/user'


export const Header = () => {
    // Ref To the new Blog popup
    const user:User = GetUser()[0]
    const img = user==undefined || user?.profile_img =="" ? "https://ideate-blog-profile-images.s3.ap-southeast-2.amazonaws.com/profile_default.png" : user.profile_img
    //TODO
    
    // const logout = () => {
    //     console.log('Logout')
    // }

  return (
    <nav className="sticky top-0 flex justify-between p-2 font-sans text-white bg-black h-14 roboto" >
        <div className='flex items-center w-1/3 min-w-[156px]' >
            <img src={ logo } alt='Slalom-Logo'/>
        </div>
        <div className='text-white items-center text-left flex flex-row w-1/3 ml-[4%] font-semibold '>
            {user && <a data-testid='home-link' className='w-1/4 px-1 displaySmall:grow hover:text-slalomBlue active:text-slalomBlue' href='/home'>HOME</a>}
            {user && <a data-testid='ourStory-link' className='w-1/4 px-1 displaySmall:grow hover:text-slalomBlue active:text-slalomBlue' href='/ourStory'>OUR STORY</a>}
            {user && <a data-testid='myBlogs-link' className='w-1/4 px-1 displaySmall:grow hover:text-slalomBlue' href='/myBlog'>MY BLOGS</a>}
            {user && user?.isAdmin && <a data-testid='admin-link' className='w-1/4 px-1 displaySmall:grow hover:text-slalomBlue' href='/admin'>ADMIN</a>}
        </div>

        <div className="flex items-center justify-end w-1/3">
            {!user && 
                <div>
                    <button onClick={() => {window.location.href = '/'}} className='bg-slalomBlue rounded-md w-[90px] h-8 mr-4'>Sign In</button>
                </div>
            }
            {user && <div className='w-10' ><p onClick={ logout } className="cursor-pointer"><Exit></Exit></p></div>}
            {user && <div className='w-6' ><p className='text-3xl font-light'>|</p></div>}
            {user && <div className='w-10'>
                <button className='align-middle'>
                    <img className="justify-center object-cover w-10 h-10 mr-2 rounded-full" src={img} alt="File not found"/>
                </button>
            </div>}
        </div>
    </nav>
  )
}
