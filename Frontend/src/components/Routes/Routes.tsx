// import React from 'react'
import { Home, PostPage, Blogs, FullBlogs, Admin, SignUp, Login} from "../../pages";
import { Route, Routes } from "react-router-dom";
import { AboutUs } from "../AboutUs";

export const RouteModel = () => {
  return (
      <Routes>
        <Route path="/posts/:id" element={<PostPage />} />
        <Route path="/myblog" element={<Blogs />} />
        <Route path="/blog" element={<FullBlogs />} />
        <Route path="/home" element={<Home />} />
        <Route path="/ourStory" element={<AboutUs />} />
        <Route path="/admin" element={<Admin />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/" element={<Login />} />
      </Routes>
  )
}
 