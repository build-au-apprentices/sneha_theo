import { createRef, useState } from 'react'
import { DeleteUserModal } from '../Modals/DeleteUserModal'
import { User } from '../../models'
import { ModalRefs } from '../../models/Modal';
import { UpdateUser } from '../../api';
import { Authenticate } from '../Cognito';

export const AdminUserTabs = ({ user } : {user : User}) => {
  // Define the ref for the Modal Delete User
  const openModalRef = createRef<ModalRefs>();
  let isAdmin = user.isAdmin
  const currentUser = Authenticate()

  const handleAdminUpdate = () => {
    console.log(isAdmin)
    isAdmin = !isAdmin
    console.log(isAdmin)
    UpdateUser(user.userId,user.firstName,user.lastName,user.pronouns,user.profile_img,isAdmin)
    if (user.userId==currentUser && !isAdmin) {
      window.location.href = "/home"
    }else{
      window.location.reload()
    }
  }


  return (
    <div>
        <span className='flex my-2 justify-between text-center items-center bg-gray-900 hover:bg-gray-800 cursor-pointer py-[5px]' >
            <input data-testid='user-select-checkbox' className='w-[6.2857%] adminDisplay:w-[25%] ' type='checkbox'/>
            <p data-testid='first-name' className='w-[18.2857%] adminDisplay:w-[25%]'>{user.firstName}</p>
            <p data-testid='last-name' className='w-[18.2857%] adminDisplay:w-[25%]'>{user.lastName}</p> 
            <p data-testid='email' className='w-[22.2857%] adminDisplay:hidden' >{user.email}</p> 
            <p data-testid='pronoun' className='w-[8.2857%] adminDisplay:hidden' >{user.pronouns}</p>
            <p data-testid='date' className='w-[20.2857%] adminDisplay:hidden' >{String(user.date_created).substring(0,10)}</p>
             <div className='flex w-[6.2857%] adminDisplay:w-[25%] justify-center align-middle text-center items-center pl-7'>
               <input type='checkbox' checked={isAdmin} onChange={() => handleAdminUpdate()} />
            </div>  
        </span>
        {<DeleteUserModal firstName={user.firstName} lastName={user.lastName} userID={user.userId} ref={openModalRef}/>}
    </div>
  )
}
