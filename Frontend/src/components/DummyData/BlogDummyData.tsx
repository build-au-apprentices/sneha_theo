import { Blog, User } from "../../models"

export const BLOGS:Blog[] = [
    {   
        blogId: 1,
        userId: "sneha_deb",
        title: 'The end of the world',
        summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        isPublished: true,
        image: '/test-images/city.jpg',
        date_created: new Date(),
        date_mod: new Date(),
        likes: 120
    },
    {
        blogId: 2,
        userId: "sneha_deb",
        title: 'Coffee spoils the new generation',
        summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse. Varius vel pharetra vel turpis nunc eget lorem. Justo eget magna fermentum iaculis. Posuere lorem ipsum dolor sit amet.',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        isPublished: true,
        image: '/test-images/coffee.jpg',
        date_created: new Date(),
        date_mod: new Date(),
        likes: 100
    },
    {
        blogId: 3,
        userId: "sneha_deb",
        title: 'In the world of a gamer',
        summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse. Varius vel pharetra vel turpis nunc eget lorem. Justo eget magna fermentum iaculis. Posuere lorem ipsum dolor sit amet. Auctor neque vitae tempus quam pellentesque. Interdum consectetur libero id faucibus nisl tincidunt eget nullam. Ut etiam sit amet nisl purus in mollis nunc sed. Est placerat in egestas erat. Id diam vel quam elementum. Imperdiet nulla malesuada pellentesque elit eget gravida. Consequat nisl vel pretium lectus quam. Nunc sed augue lacus viverra vitae. Tellus rutrum tellus pellentesque eu. Montes nascetur ridiculus mus mauris vitae ultricies leo integer. ',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        isPublished: true,
        image: '/test-images/computer.jpg',
        date_created: new Date(),
        date_mod: new Date(),
        likes: 150
    },
    {
        blogId: 4,
        userId: "sneha_deb",
        title: 'Increase in car prices',
        summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        isPublished: true,
        image: '/test-images/cars.jpg',
        date_created: new Date(),
        date_mod: new Date(),
        likes: 105
    },
    {
        blogId: 5,
        userId: "sneha_deb",
        title: 'Beach Getaways',
        summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        isPublished: true,
        image: '/test-images/beach.png',
        date_created: new Date(),
        date_mod: new Date(),
        likes: 67
    }
]


export const USERS:User[] = [
    {
        userId: 'theo_pol',
        firstName: 'Theodore',
        lastName: 'Politis',
        email: 'theo.politis@slalom.com',
        pronouns: 'He/Him',
        date_created: new Date(),
        date_mod: new Date(),
        isAdmin: false,
        profile_img: ""
    },
    {
        userId: 'sneha_deb',
        firstName: 'Sneha',
        lastName: 'Debsikdar',
        email: 'snehadebsikdar@gmail.com',
        pronouns: 'She/Her',
        date_created: new Date(),
        date_mod: new Date(),
        isAdmin: false,
        profile_img: ""
    } ]
//     {
//         id: 3,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 4,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 5,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 6,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 7,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 8,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 9,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 10,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 11,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 12,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 13,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 14,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 15,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 16,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 17,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 18,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 19,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 20,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 21,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 22,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 23,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 24,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 25,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 26,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 27,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 28,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 29,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 30,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 31,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 32,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     },
//     {
//         id: 33,
//         firstName: 'Theodore',
//         lastName: 'Politis',
//         email: 'theo.politis@slalom.com',
//         pronouns: 'Him',
//         dateCreated: new Date()
//     },
//     {
//         id: 34,
//         firstName: 'Sneha',
//         lastName: 'Debskidar',
//         email: 'sneha.debskidar@slalom.com',
//         pronouns: 'Her',
//         dateCreated: new Date()
//     }
// ]