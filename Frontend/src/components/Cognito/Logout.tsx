// import Pool from "../../UserPool";
import { Auth } from "aws-amplify";

export const logout = () => {
  async function signOut() {
    try {
      await Auth.signOut().then(() => {
        console.log("signed out !");
        window.location.href = "/";
      });
    } catch (error) {
      console.log("error signing out: ", error);
    }
  }
  signOut();
};
