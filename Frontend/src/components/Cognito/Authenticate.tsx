import { Auth } from "aws-amplify";
import { useEffect, useState } from "react";

export const Authenticate = () => {

  const [user, setUser] = useState("");
  useEffect(() => {
  async function onload() {
    try {
      const users = await Auth.currentAuthenticatedUser({
        bypassCache: true,
      });
      console.log(users.username);
      setUser(users.username);
    } catch (error) {
      //alert(error);
    }
  }
  onload();
  
}, [user]);

  return user

};
