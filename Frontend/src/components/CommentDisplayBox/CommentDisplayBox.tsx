import { TrashIcon } from '@heroicons/react/outline'
import { Auth } from 'aws-amplify'
import React, { useEffect, useState } from 'react'
import {Comment } from '../../models'
import { DeleteComment } from '../../api/comment'

interface CommentProps {
    comment : Comment
}

export const CommentDisplayBox: React.FC<CommentProps> = (props,comment:Comment): JSX.Element => {
  const [isOwner, setIsOwner] = useState(false)

  useEffect(() => {
    Auth.currentAuthenticatedUser()
    .then(data => {
      if(data.username === props.comment.userId){
        setIsOwner(true)
      }
    })
    .catch(error => {
      console.log(error)
    })
  }, [props.comment.userId])

  const onDelete = () => {
    DeleteComment(props.comment.commentId)
    console.log('Delete')
    window.location.reload()
  }

  return (
    <div className='w-[100%] flex flex-col items-start text-xs pr-3 border-b-[1px] border-l-[1px] border-slalomBlue mt-2 pl-1'>
      <div className='flex flex-row justify-between w-full'>
        <p className='text-[11px]'>{props.comment.userId}</p>
        {isOwner && <TrashIcon onClick={() => onDelete()} className='w-[15px] text-red-600 cursor-pointer hover:text-red-300'/>}
      </div>
      <p className='pb-1'>{props.comment.message}</p>
      <p className='text-[11px] self-end'>{String(props.comment.date_created).substring(0, 10)}</p>

    </div>
  )
}
