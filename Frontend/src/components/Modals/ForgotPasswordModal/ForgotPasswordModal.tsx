import { Dialog, Transition } from "@headlessui/react";
import React, { forwardRef, Fragment, useImperativeHandle, useRef, useState } from "react";
import { ModalRefs } from "../../../models/Modal";
import { Auth } from "aws-amplify";
import { XIcon } from "@heroicons/react/outline"


// Props specific to component
interface NewBlogProps {
}

export const ForgotPasswordModal = forwardRef<ModalRefs, NewBlogProps>((props, ref) => {
    const [open, setOpen] = useState(false);
    const [verification, setVerification] = useState(false)
    const [valError, setValError] = useState('')
    const [verificationError, setVerificationError] = useState('')
    const [username, setUsername] = useState('')
    const [verificationCode, setVerificationCode] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [showPassword, setShowPassword] = useState('password')
    const cancelButtonRef = useRef(null);

    // Defining the function we want to call from ref
    useImperativeHandle(ref, () => ({
        openDialog() {
            openModal()
        }
    }))

    const reset = () => {
        closeModal()
        setVerification(false)
        setValError('')
        setVerificationError('')
        setUsername('')
        setVerificationCode('')
        setPassword('')
        setConfirmPassword('')
    }

    // Open / Close function of Modal
    const openModal = () => {
        setOpen(true)
    }

    const closeModal = () => {
        setOpen(false)  
    }
    const onSendEmail = () => {
        Auth.forgotPassword(username)
        .then(data => {
            setVerification(true)
        })
        .catch(error => {
            const strErr = String(error)
            if(strErr.includes('Username/client id combination not found.')){
                setValError('Username does not exist')
            }else if (strErr.includes('Attempt limit exceeded, please try after some time.')){
                setValError('Attempt limit exceeded, please try after some time.')
            }

            console.log(error)
        })
    }

    const onVerify = () => {
        if(password === confirmPassword){
            Auth.forgotPasswordSubmit(username, verificationCode, password)
            .then(data => {
                reset()
                alert('Password Change Successful')
            })
            .catch(error => {
                const strErr = String(error)
                if(strErr.includes('Invalid verification code provided, please try again.')){
                    setVerificationError('Invalid verification code provided, please try again.')
                }else if(strErr.includes('Password must have symbol characters')){
                    setVerificationError('Password must have symbol characters')
                }else if(strErr.includes('Password must have numeric characters')){
                    setVerificationError('Password must have numeric characters')
                }else if(strErr.includes('Password must have uppercase characters')){
                    setVerificationError('Password must have uppercase characters')
                }else if(strErr.includes('Password not long enough')){
                    setVerificationError('Password not long enough')
                }else if(strErr.includes('Password must have lowercase characters')){
                    setVerificationError('Password must have lowercase characters')
                }
                console.log(error)
            })
        }else{
            setVerificationError('Passwords do not match')
        }
    }

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog
                as="div"
                className="fixed inset-0 z-10 overflow-y-auto"
                initialFocus={cancelButtonRef}
                onClose={reset}
            >
                <div className="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Dialog.Overlay className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
                    </Transition.Child>

                    {/* This element is to trick the browser into centering the modal contents. */}
                    <span
                        className="hidden sm:inline-block sm:align-middle sm:h-screen"
                        aria-hidden="true"
                    >
                        &#8203;
                    </span>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        enterTo="opacity-100 translate-y-0 sm:scale-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                        leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    >   
                    
                        <div className="inline-block align-middle transition-all border-[1px] border-black transform bg-white rounded-lg shadow-xl w-[30%] displaySmall:h-[600px] displaySmall:w-[95%] max-h-[80%] h-[80%] p-2">
                            <div className="flex flex-row-reverse ">
                                <XIcon onClick={reset} className="cursor-pointer w-7 opacity-60 hover:opacity-100"/>
                            </div>
                           {!verification && <div className="flex flex-col items-center my-[100px]">
                               <h1 className="py-1 mb-2 text-2xl border-b-2">Forgot your password</h1>
                               <h3 className="mb-1 text-red-600">{valError}</h3>
                               <input onChange={(e) => setUsername(e.target.value)} value={username} placeholder="Username" className="border-[1px] w-[80%] h-8 rounded-md border-slate-400 pl-1"/>
                               <button onClick={() => onSendEmail()} className="h-8 mx-2 text-white rounded-md bg-slalomBlue w-[100px] mt-4 drop-shadow-md hover:bg-sky-300">Submit</button>
                           </div>}
                           {verification &&
                                <div className="flex flex-col items-center my-[100px] gap-2">
                                    <h1 className="py-1 text-xl border-b-2 w-[80%]">Verification code has been sent to your email</h1>
                                    <h3 className="text-red-600 ">{verificationError}</h3>
                                    <input onChange={(e) => setVerificationCode(e.target.value)} value={verificationCode} placeholder="Verification Code" className="pl-1 border-[1px] w-[80%] text-center h-8 rounded-md border-slate-400"/>
                                    <input type={showPassword} onChange={(e) => setPassword(e.target.value)} value={password} placeholder="Password" className="pl-1 border-[1px] w-[80%] h-8 rounded-md border-slate-400 text-center"/>
                                    <input type={showPassword} onChange={(e) => setConfirmPassword(e.target.value)} value={confirmPassword} placeholder="Confirm Password" className="pl-1 border-[1px] w-[80%] h-8 rounded-md text-center border-slate-400"/>
                                    <div className="flex flex-row items-center">
                                        <label className="px-2">Show password: </label>
                                        <input type='checkbox' onClick={() => showPassword === 'text' ? setShowPassword('password') : setShowPassword('text')} className="w-5 mx-1 rounded-md"/>
                                    </div>
                                    <button onClick={() => onVerify()} className="h-8 mx-2 text-white rounded-md bg-slalomBlue w-[170px] mt-4 drop-shadow-md hover:bg-sky-300">Change Password</button>
                                </div>
                           }
                        </div>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition.Root>
    );
}
)
