import { forwardRef, Fragment, useImperativeHandle, useRef, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { ExclamationIcon } from '@heroicons/react/outline'
import { ModalRefs } from '../../../models/Modal'

// Props Specific to Component
export interface DeleteUserProps {
    firstName: string,
    lastName: string,
    userID: string
}

export const DeleteUserModal = forwardRef<ModalRefs, DeleteUserProps>((props, ref) => {
    const [open, setOpen] = useState(false)
    const [deleteVal, setDeleteVal] = useState('')
    const [invalidInput, setInvalidInput] = useState('')
    const cancelButtonRef = useRef(null)

    // Validates and checks that DELETE has been entered in input wont close screen until correct input
    const onDeactivate = () => {
        if(deleteVal === 'DELETE')
        {
            closeModal()
        }
        else{
            setInvalidInput('* Invalid Input')
        }
    }

    // Open / Close functions of Modal
    const openModal = () => {
        setOpen(true)
    }

    const closeModal = () => {
        setOpen(false)
    }
    
    // Define functions for the ref so parent can open Modal
    useImperativeHandle(ref, () => ({
        openDialog() {
            openModal();
        }
    }));

    return (
    <Transition.Root show={open} as={Fragment}>
        <Dialog as="div" className="fixed inset-0 z-10 overflow-y-auto" initialFocus={cancelButtonRef} open={open} onClose={closeModal}>
        <div className="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
            >
            <Dialog.Overlay className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
            &#8203;
            </span>
            <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
            <div className="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                <div className="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                <div className="sm:flex sm:items-start">
                    <div className="flex items-center justify-center flex-shrink-0 w-12 h-12 mx-auto bg-red-100 rounded-full sm:mx-0 sm:h-10 sm:w-10">
                    <ExclamationIcon className="w-6 h-6 text-red-600" aria-hidden="true" />
                    </div>
                    <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                    <Dialog.Title as="h3" className="text-lg font-medium leading-6 text-gray-900">
                        Delete account
                    </Dialog.Title>
                    <div>
                        <p>Do you wish to delete the following account?</p>
                    </div>
                    <div className='py-2 text-center border-[1px] border-slate-400'>
                        <p data-testid='user-name-display'>{props.firstName} {props.lastName}</p>
                    </div>
                    <div className="flex flex-col mt-2">
                        <div className='text-xs text-right text-red-600 '>
                            <p data-testid='invalid-input-error' >{invalidInput}</p>
                        </div>
                        <div className='flex flex-row ml-4'>
                            <p className="text-sm text-center ">Type DELETE in field to confirm:</p>
                            <input data-testid='delete-input-box' type='text' onChange={e => setDeleteVal(e.target.value)} className='border-[1px] border-slate-400 grow'></input>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div className="px-4 py-3 bg-gray-50 sm:px-6 sm:flex sm:flex-row-reverse">
                <button
                    data-testid='deactivate-button'
                    type="button"
                    className="inline-flex justify-center w-full px-4 py-2 text-base font-medium text-white bg-red-600 border border-transparent rounded-md shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm"
                    onClick={() => onDeactivate()}
                >
                    Deactivate
                </button>
                <button
                    type="button"
                    className="inline-flex justify-center w-full px-4 py-2 mt-3 text-base font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                    onClick={closeModal}
                    ref={cancelButtonRef}
                >
                    Cancel
                </button>
                </div>
            </div>
            </Transition.Child>
        </div>
        </Dialog>
    </Transition.Root>
  );
}
)
