// import { fireEvent, render, screen } from "@testing-library/react";
// import { createRef } from "react";
export {}
// import { act } from "react-dom/test-utils";
// import { ModalRefs } from "../../../models/Modal";
// import { MockIntersectionObserver } from "../../../SetupTests/Mocks";
// import { DeleteUserModal } from "./DeleteUserModal";

// test('deactivateWithDeleteAsKeyword_Error', () => {
//     const ref = createRef<ModalRefs>();
//     // Mock the Window Observer so that it is defined
//     window.IntersectionObserver = jest.fn().mockImplementation(MockIntersectionObserver)

//     render(<DeleteUserModal firstName={''} lastName={''} userID={1} ref={ref}/>)

//     // Make sure Modal is visible else test-id is useless
//     act(() => {
//         ref.current?.openDialog()
//     })

//     // Elements Used for the test
//     const input = screen.getByTestId('delete-input-box')
//     const deactivateButton = screen.getByTestId('deactivate-button')
//     const error = screen.getByTestId('invalid-input-error')

//     // Simulate Events
//     fireEvent.change(input, {target: {value: 'delete'}})
//     fireEvent.click(deactivateButton)

//     expect(error.innerHTML).toBe('* Invalid Input')
// })

// test('CheckNoErrorIsDisplayedWithNoInput_Error', () => {
//     const ref = createRef<ModalRefs>();

//     window.IntersectionObserver = jest.fn().mockImplementation(MockIntersectionObserver)

//     render(<DeleteUserModal firstName={''} lastName={''} userID={1} ref={ref}/>)

//     act(() => {
//         ref.current?.openDialog()
//     })

//     const input = screen.getByTestId('delete-input-box')
//     const deactivateButton = screen.getByTestId('deactivate-button')
//     const error = screen.getByTestId('invalid-input-error')

//     fireEvent.change(input, {target: {value: ''}})
//     fireEvent.click(deactivateButton)

//     expect(error.innerHTML).toBe('* Invalid Input')
// })

// test('CheckNoErrorIsDisplayedWithCorrectInput_NoError', () => {
//     const ref = createRef<ModalRefs>();

//     window.IntersectionObserver = jest.fn().mockImplementation(MockIntersectionObserver)

//     render(<DeleteUserModal firstName={''} lastName={''} userID={1} ref={ref}/>)

//     act(() => {
//         ref.current?.openDialog()
//     })

//     const input = screen.getByTestId('delete-input-box')
//     const deactivateButton = screen.getByTestId('deactivate-button')
//     const error = screen.getByTestId('invalid-input-error')

//     fireEvent.change(input, {target: {value: 'DELETE'}})
//     fireEvent.click(deactivateButton)

//     expect(error.innerHTML).toBe('')
// })

// test('CheckUserDataIsDisplayed_JohnDoe', () => {
//     const FIRST_NAME = 'John'
//     const LAST_NAME = 'Doe'

//     const ref = createRef<ModalRefs>();

//     window.IntersectionObserver = jest.fn().mockImplementation(MockIntersectionObserver)
//     render(<DeleteUserModal firstName={FIRST_NAME} lastName={LAST_NAME} userID={1} ref={ref}/>)
//     act(() => {
//         ref.current?.openDialog()
//     })

//     const name = screen.getByTestId('user-name-display')

    

//     expect(name.innerHTML).toBe(FIRST_NAME + ' ' + LAST_NAME)
// })