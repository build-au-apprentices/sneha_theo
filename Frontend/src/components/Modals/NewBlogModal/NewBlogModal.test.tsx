import { fireEvent, render, screen } from "@testing-library/react";
import { createRef } from "react";
import { act } from "react-dom/test-utils";
import { ModalRefs } from "../../../models/Modal";
import { MockIntersectionObserver } from "../../../SetupTests/Mocks";
import { NewBlogModal } from "./NewBlogModal"

const setup = () => {
    const ref = createRef<ModalRefs>();
    window.IntersectionObserver = jest.fn().mockImplementation(MockIntersectionObserver)
    const utils = render(<NewBlogModal isNew={true} ref={ref}/>)
    act(() => {
        ref.current?.openDialog()
    })

    return utils
}

test('NoTitleEnteredWithPublishButtonOne_displayErrorMessage', () => {
    setup()

    // Component Elements used for test
    const publishButtonOne = screen.getByTestId('publish-button-one')
    const error = screen.getByTestId('error-message')

    // Simulate Action
    fireEvent.click(publishButtonOne)

    //Expected Outcome
    expect(error.innerHTML).toBe('Title cannot be blank')
})

test('NoTitleEnteredWithPublishButtonTwo_displayErrorMessage', () => {
    setup()

    const publishButtonTwo = screen.getByTestId('publish-button-two')
    const error = screen.getByTestId('error-message')

    fireEvent.click(publishButtonTwo)

    expect(error.innerHTML).toBe('Title cannot be blank')
})

test('NoSummaryEnteredWithPublishButtonOne_displayErrorMessage', () => {
    setup()

    const titleInput = screen.getByTestId('title-input')
    const publishButtonOne = screen.getByTestId('publish-button-one')
    const error = screen.getByTestId('error-message')

    fireEvent.change(titleInput, {target: {value: 'Hello World'}})
    fireEvent.click(publishButtonOne)

    expect(error.innerHTML).toBe('Summary cannot be blank')
})

test('NoSummaryEnteredWithPublishButtonTwo_displayErrorMessage', () => {
    setup()

    const titleInput = screen.getByTestId('title-input')
    const publishButtonTwo = screen.getByTestId('publish-button-two')
    const error = screen.getByTestId('error-message')

    fireEvent.change(titleInput, {target: {value: 'Hello World'}})
    fireEvent.click(publishButtonTwo)

    expect(error.innerHTML).toBe('Summary cannot be blank')
})

test('NoTextEnteredWithPublishButtonOne_displayErrorMessage', () => {
    setup()

    const titleInput = screen.getByTestId('title-input')
    const summaryInput = screen.getByTestId('summary-input')
    const publishButtonOne = screen.getByTestId('publish-button-one')
    const error = screen.getByTestId('error-message')

    fireEvent.change(titleInput, {target: {value: 'Hello World'}})
    fireEvent.change(summaryInput, {target: {value: 'Hello World'}})
    fireEvent.click(publishButtonOne)

    expect(error.innerHTML).toBe('Text cannot be blank')
})

test('NoTextEnteredWithPublishButtonTwo_displayErrorMessage', () => {
    setup()

    const titleInput = screen.getByTestId('title-input')
    const summaryInput = screen.getByTestId('summary-input')
    const publishButtonTwo = screen.getByTestId('publish-button-two')
    const error = screen.getByTestId('error-message')

    fireEvent.change(titleInput, {target: {value: 'Hello World'}})
    fireEvent.change(summaryInput, {target: {value: 'Hello World'}})
    fireEvent.click(publishButtonTwo)

    expect(error.innerHTML).toBe('Text cannot be blank')
})

test('PressingCancelSetsErrorMessageToEmptyWhenValueExists_ErrorMessageToBeEmpty', () => {
    setup()

    const publishButtonOne = screen.getByTestId('publish-button-one')
    const cancelButton = screen.getByTestId('cancel-button')
    const error = screen.getByTestId('error-message')

    fireEvent.click(publishButtonOne)
    expect(error.innerHTML).toBe('Title cannot be blank')

    fireEvent.click(cancelButton)
    expect(error.innerHTML).toBe('')
})

test('PressingCancelSetsTitleInputToEmpty_TitleToBeEmpty', () => {
    setup()

    const cancelButton = screen.getByTestId('cancel-button')
    const input = screen.getByTestId('title-input')

    fireEvent.change(input, {target: {value: 'Hello World'}})

    fireEvent.click(cancelButton)
    expect(input.getAttribute('value')).toBe('')
    
})

test('PressingCancelSetsSummaryInputToEmpty_SummaryToBeEmpty', () => {
    setup()

    const cancelButton = screen.getByTestId('cancel-button')
    const summaryInput = screen.getByTitle('summary-input')

    fireEvent.change(summaryInput, {target: {value: 'Hello Summary'}})

    fireEvent.click(cancelButton)
    expect(summaryInput.textContent).toBe('')
})

test('PressingCancelSetsTextInputToEmpty_TextToBeEmpty', () => {
    setup()

    const cancelButton = screen.getByTestId('cancel-button')
    const textInput = screen.getByTestId('text-input')

    fireEvent.change(textInput, {target: {value: 'Hello World'}})

    fireEvent.click(cancelButton)
    expect(textInput.textContent).toBe('')
})