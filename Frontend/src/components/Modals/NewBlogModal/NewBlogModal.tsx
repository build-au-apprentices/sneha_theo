import { Dialog, Transition } from "@headlessui/react";
import React, { forwardRef, Fragment, useEffect, useImperativeHandle, useRef, useState } from "react";
import { NewspaperIcon } from '@heroicons/react/outline'
import { ModalRefs } from "../../../models/Modal";
import { Blog } from "../../../models";
import {Authenticate} from "../../Cognito/Authenticate";
import {CreateBlogs} from "../../../api/blogs";
import {UpdateBlogs} from "../../../api/blogs";



// Props specific to component
interface NewBlogProps {
    blog?: Blog
    isNew: boolean
}

export const NewBlogModal = forwardRef<ModalRefs, NewBlogProps>((props, ref) => {
    const [open, setOpen] = useState(false);
    const cancelButtonRef = useRef(null);
    const user = Authenticate();
    // Value states
    //const [published,setPublished] = useState(false)
    let published = props.blog !== undefined ? props.blog.isPublished : false
    const [title, setTitle] = useState(props.blog !== undefined ? props.blog.title : 'Enter a Title')
    const [summary, setSummary] = useState(props.blog !== undefined ? props.blog.summary : 'Enter a Summary')
    const [text, setText] = useState(props.blog !== undefined ? props.blog.content : 'Enter some text')
    const [keywords, setKeywords] = useState('')
    const [selectedFile, setSelectedFile] = useState<File>()
    const [errorMessage, setErrorMessage] = useState('')
    const [filename, setFilename] = useState(props.blog !== undefined ? props.blog.image :"https://ideate-blog-profile-images.s3.ap-southeast-2.amazonaws.com/blog_default.webp")

    
    // Defining the function we want to call from ref
    useImperativeHandle(ref, () => ({
        openDialog() {
            openModal()
        }
    }))

    // Open / Close function of Modal
    const openModal = () => {
        setOpen(true)
    }

    const closeModal = () => {
        setOpen(false)
        if(props.isNew){
            setErrorMessage('')
            setTitle('')
            setSummary('')
            setText('')
            setSelectedFile(undefined)
        }else{
            setErrorMessage('')
        }
    }

    // Saves the file to state
    const handleFileChange = (event : React.FormEvent) => {
        const files = (event.target as HTMLInputElement).files
       
        if(files && files.length > 0){
            console.log("file is uploaded")
            //setSelectedFile(files[0])
            uploadImage(files[0]);
        }
    }   

    const uploadImage = async (file:File) => {
        const { url } = await fetch("http://3.106.166.81:8080/s3Url").then(res => res.json())
        console.log(url)
        
        setFilename(url.split('?')[0]);
        //await axios.put(url,file)
        await fetch(url, {
            method: "PUT",
            headers: {
              "Content-Type": "multipart/form-data"
            },
            body: file
        })
        
    }
    // Validates and then closes modal
    const onPublish = () => {
        if(validate()){
            console.log("publishing")
            published = true
            console.log(published)
            onSave()
        }
        //closeModal()
    }

    // Validates input fields
    const validate = () =>{
        if(title.length === 0){
            setErrorMessage('Title cannot be blank')
            return false
        }

        if(summary.length === 0){
            setErrorMessage('Summary cannot be blank')
            return false
        }

        if(text.length === 0){
            setErrorMessage('Text cannot be blank')
            return false;
        }

        return true
    }
    
    const onSave = (file?:File) => {
        if (props.isNew){
            CreateBlogs(title,summary,text,filename,0,user,published);
        }else{
            console.log("hello");
            const blogId = props.blog?.blogId
            if(blogId){
                UpdateBlogs(blogId,title,summary,text,filename,0,user,published);
            }
           
        }
        window.location.reload();
    }
    
     

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog
                as="div"
                className="fixed inset-0 z-10 overflow-y-auto"
                initialFocus={cancelButtonRef}
                onClose={closeModal}
            >
                <div className="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Dialog.Overlay className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
                    </Transition.Child>

                    {/* This element is to trick the browser into centering the modal contents. */}
                    <span
                        className="hidden sm:inline-block sm:align-middle sm:h-screen"
                        aria-hidden="true"
                    >
                        &#8203;
                    </span>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        enterTo="opacity-100 translate-y-0 sm:scale-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                        leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    >
                        <div className="inline-block overflow-y-scroll text-left align-middle transition-all transform bg-white rounded-lg shadow-xl w-[65%] displaySmall:h-[600px] displaySmall:w-[95%] max-h-[80%] px-2">
                            <div className="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                                <div className="flex flex-row justify-between">
                                    <div className="flex flex-row items-center mx-2">
                                        <NewspaperIcon className="w-[30px] h-[30px]" />
                                        <Dialog.Title as='h2' className='px-2 text-lg font-medium leading-6 text-gray-900 justify-self-center'> Create a New Blog</Dialog.Title>
                                    </div>
                                    <div className="flex mr-[8%] items-center">
                                        <p data-testid='error-message' className='text-sm text-red-600'>{errorMessage}</p>
                                    </div>
                                </div>
                                <div className="flex flex-col px-2">
                                    <div className="flex flex-row ">
                                        <div className="flex flex-col w-[60%] displaySmall:w-full ">
                                            <input data-testid='title-input' onChange={e => setTitle(e.target.value)} value={title} type='text' placeholder="Title" className="border px-2 border-slate-300 w-[70%] displaySmall:w-full my-2 rounded-md" />
                                            <div className="w-[70%] justify-self-center displaySmall:contents hidden">
                                                <h3 className="mt-2 text-center text-md">Header Image</h3>
                                                <input value={selectedFile?.name} onClick={e => handleFileChange(e)} className="block w-full px-2 py-1 text-sm text-gray-700 transition ease-in-out bg-white border border-gray-300 border-solid rounded form-control bg-clip-padding focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" id="formFileSm" type="file" />
                                            </div>
                                            <textarea title='summary-input' data-testid='summary-input' onChange={e => setSummary(e.target.value)} value={summary} placeholder='Summary' className='border border-slate-300 w-[100%] mt-6 mb-4 lg:h-[125px] displaySmall:h-[150px] resize-none rounded-md px-1 leading-tight' />
                                        </div>
                                        <div className="flex flex-col items-center justify-items-stretch grow displaySmall:hidden">
                                            <div className="w-[70%] justify-self-center">
                                                <h3 className="mt-2 text-center text-md">Header Image</h3>
                                                <input data-testid='image-input'value={selectedFile?.name} onChange={e => handleFileChange(e)} className="block w-full px-2 py-1 text-sm text-gray-700 transition ease-in-out bg-white border border-gray-300 border-solid rounded form-control bg-clip-padding focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" id="formFileSm" type="file" />
                                            </div>
                                            <div className="w-[100%] mt-4">
                                                <textarea data-testid='keyword-input-one' onChange={e => setKeywords(e.target.value)} value={keywords} placeholder='Tags / Keywords' className='block ml-auto mr-auto px-1 my-2 border rounded-md resize-none w-[90%] items-center border-slate-300' />
                                            </div>
                                            <div className="p-2 my-2">
                                                <button data-testid='publish-button-two' onClick={onPublish} className='w-[65px] text-white bg-sky-600 hover:bg-sky-500 rounded-lg drop-shadow-md displaySmall:flex' >Publish</button>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="" >
                                        <textarea data-testid='text-input' onChange={e => setText(e.target.value)} value={text} placeholder='Text' className='border border-slate-300 w-[100%] my-2 h-[400px] resize-none rounded-md px-1' />
                                    </div>
                                    <div className="w-[100%] mt-4 displaySmall:contents hidden ">
                                        <textarea data-testid='keyword-input-two' onChange={e => setKeywords(e.target.value)} value={keywords} placeholder='Tags / Keywords' className='block ml-auto mr-auto px-1 my-2 border rounded-md resize-none w-[90%] items-center border-slate-300' />
                                    </div>
                                    <div className="flex flex-row justify-evenly">
                                        <button data-testid='cancel-button' className='w-[65px] text-white bg-red-600 rounded-lg drop-shadow-md hover:bg-red-400' onClick={closeModal}>Cancel</button>
                                        <button onClick={()=>onSave()} className='w-[65px] text-white bg-sky-600 hover:bg-sky-500 rounded-lg drop-shadow-md' >Save</button>
                                        <button data-testid='publish-button-one' onClick={onPublish} className='w-[65px] text-white bg-sky-600 hover:bg-sky-500 rounded-lg drop-shadow-md hidden displaySmall:block' >Publish</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition.Root>
    );
}
)
