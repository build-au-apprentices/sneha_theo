import { Dialog, Transition } from "@headlessui/react";
import { forwardRef, Fragment, useImperativeHandle, useRef, useState } from "react";
import { HeartIcon, XIcon } from '@heroicons/react/solid'
import { ModalRefs } from "../../../models/Modal";
import { Blog, User } from "../../../models";
import { Comment } from "../../../models";
import { CommentDisplayBox } from "../../CommentDisplayBox";
import { CommentCreate } from "../../CommentCreate";
import { GetComments } from "../../../api/comment";
import { GetUser } from "../../../api/user";



// Props specific to component
interface NewBlogProps {
    blog: Blog
    isDraft?: boolean
}

export const BlogViewModal = forwardRef<ModalRefs, NewBlogProps>((props, ref) => {
    const [open, setOpen] = useState(false);
    const cancelButtonRef = useRef(null);
    const comments = GetComments(props.blog.blogId);
    const user:User = GetUser(props.blog?.userId)[0]
    const invisible:String = props.isDraft ? "invisible" : ""

    //console.log(comments[0])
    // Defining the function we want to call from ref
    useImperativeHandle(ref, () => ({
        openDialog() {
            openModal()
        }
    }))

    // Open / Close function of Modal
    const openModal = () => {
        setOpen(true)
    }

    const closeModal = () => {
        setOpen(false)
    }

    function getcomments() {
        return comments.map((comm: Comment) => {
            <div className="w-[100%] flex flex-col items-start text-xs pr-3 border-b-[1px] border-l-[1px] border-slalomBlue mt-2 pl-1">
                <div className="flex flex-row justify-between w-full">
                    <p className="text-[11px]">{comm.userId}</p>
                    {console.log(comm.userId)}
                    {/* {isOwner && <TrashIcon } className='w-[15px] text-red-600 cursor-pointer hover:text-red-300'/>} */}
                </div>
                <p className="pb-1">{comm.message+"h"}</p>
                {console.log(comm.message + "hereeeeeee")}
                <p className="text-[11px] self-end">{comm.date_created}</p>
            </div>;

        });
    }

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog
                as="div"
                className="fixed inset-0 z-10 overflow-y-auto"
                initialFocus={cancelButtonRef}
                onClose={closeModal}
            >
                <div className="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Dialog.Overlay className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
                    </Transition.Child>

                    {/* This element is to trick the browser into centering the modal contents. */}
                    <span
                        className="hidden sm:inline-block sm:align-middle sm:h-screen"
                        aria-hidden="true"
                    >
                        &#8203;
                    </span>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        enterTo="opacity-100 translate-y-0 sm:scale-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                        leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    >
                        <div className=" inline-block align-middle transition-all border-[1px] border-black transform bg-white rounded-lg shadow-xl w-[70%] displaySmall:h-[600px] displaySmall:w-[95%] max-h-[80%] h-[80%]">
                            <div className="flex flex-row bg-white h-[95vh]">
                                <div className="w-[29%] bg-black text-white pt-1 pb-[3px] pl-6 flex flex-col">
                                    <h1 className="text-[19px] font-medium text-left">
                                        Comments
                                    </h1>
                                    <hr className="border-t-[2px] border-white" />
                                    <div className="overflow-y-scroll  h-[80%]">
                                        {comments.map((comment : Comment) => {
                                            return <CommentDisplayBox key={comment.commentId} comment={comment}/>
                                        })}
                                    </div>
                                    <div className={"" + invisible} >
                                        <CommentCreate blogId={props.blog?.blogId} />
                                    </div>
                                </div>
                                <div className="w-[71%] overflow-y-scroll flex flex-row">
                                    <div className="sticky top-0 order-last pt-4 pr-4">
                                        <XIcon
                                            onClick={closeModal}
                                            className="sticky top-0 cursor-pointer opacity-60 w-7 hover:opacity-100"
                                        />
                                    </div>
                                    <div className="flex flex-col">
                                        <div className="flex flex-row justify-between drop-shadow-2xl">
                                            <div className="flex flex-row mx-[5%]">
                                                <img
                                                    className=" drop-shadow-md my-4 object-cover rounded-full w-[60px] h-[60px]"
                                                    src={user?.profile_img}
                                                    alt="File not found"
                                                />
                                                <div className="flex flex-col justify-center mx-3 drop-shadow-md">
                                                    <p className="text-[15px] font-semibold">
                                                        {props.blog?.userId}
                                                    </p>
                                                    <p className="text-[11px]">
                                                        {String(props.blog?.date_created).substring(0,10)}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 className="self-start text-4xl font-medium ml-[5%] my-2">
                                            {props.blog?.title}
                                        </h1>
                                        <img
                                            src={props.blog?.image}
                                            alt="Header"
                                            className="w-[90%] h-[220px] object-cover drop-shadow-2xl self-center"
                                        />
                                        <hr className="border-[1px] border-black mx-[3%] mt-3" />
                                        <div className="text-left mx-[4%] mt-3 mb-28 text-sm whitespace-pre-wrap">
                                            <p>{props.blog?.content}</p>
                                        </div>

                                        {/* Main Body */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition.Root>
    );
}
)
