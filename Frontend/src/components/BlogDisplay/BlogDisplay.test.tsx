import { render, screen } from "@testing-library/react";
import { Blog } from "../../models";
import { BlogDisplay } from "./BlogDisplay";
import react from 'react'

// const seed:Blog = {
//     userId: 'theo_politis',
//     blogId: 3,
//     title: 'In the world of a gamer',
//     summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse. Varius vel pharetra vel turpis nunc eget lorem. Justo eget magna fermentum iaculis. Posuere lorem ipsum dolor sit amet. Auctor neque vitae tempus quam pellentesque. Interdum consectetur libero id faucibus nisl tincidunt eget nullam. Ut etiam sit amet nisl purus in mollis nunc sed. Est placerat in egestas erat. Id diam vel quam elementum. Imperdiet nulla malesuada pellentesque elit eget gravida. Consequat nisl vel pretium lectus quam. Nunc sed augue lacus viverra vitae. Tellus rutrum tellus pellentesque eu. Montes nascetur ridiculus mus mauris vitae ultricies leo integer. ',
//     content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
//     isPublished: true,
//     image: '/test-images/computer.jpg',
//     date_created: new Date(),
//     date_mod: new Date(11),
//     likes: 150
//     }


// test('Title is displayed', () => {
//     render(<BlogDisplay blog={seed} />)

//     const title = screen.getByTestId('title-display')

//     expect(title.innerHTML).toBe(seed.title)
// })

// test('Summary is displayed', () => {
//     render(<BlogDisplay blog={seed} />)

//     const summary = screen.getByTestId('text-display')

//     expect(summary.innerHTML).toBe(seed.summary)
// })

// test('likes is displayed', () => {
//     render(<BlogDisplay blog={seed} />)

//     const likes = screen.getByTestId('likes-amount-display')

//     expect(likes.innerHTML).toBe('150')
// })

// test('date is displayed', () => {
//     render(<BlogDisplay blog={seed} />)

//     const date = screen.getByTestId('date-display')

//     expect(date.innerHTML).toBe(seed.date_created)
// })

// test('Author is displayed', () => {
//     render(<BlogDisplay blog={seed} />)

//     const author = screen.getByTestId('author-display')

//     expect(author.innerHTML).toBe('Author: ' + seed.userId)

// })

