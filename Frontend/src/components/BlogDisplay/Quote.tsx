export const Quote = () => {
    return (
      <div className="flex flex-row h-screen bg-black text-white">
        <div className="flex-none w-1/3 min-w-[380px] xl:min-w-[490px] 2xl:min-w-[600px] displaySmall:hidden">
          <div className="ml-[13%] xl:ml-[8%] mt-[40%] mb-[60%] 2xl:min-w-[570px]">
            <h1 className="2xl:text-6xl xl:text-5xl text-4xl mt-1">
              Everything begins
            </h1>
            <h2 className="2xl:text-5xl 2xl:ml-[280px] 2xl:pt-2 xl:text-4xl xl:ml-[230px] text-3xl ml-[150px]">
              with an idea.
            </h2>
            <h3 className="2xl:ml-[450px] xl:ml-[335px] ml-[220px] text-sm">
              Earl Nightingale
            </h3>
          </div>
        </div>
  
      
  
      </div>
    );
  };