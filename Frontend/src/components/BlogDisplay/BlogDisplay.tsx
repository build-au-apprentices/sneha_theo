import React, { createRef, useEffect } from "react";
import { Blog } from "../../models";
import { useState } from "react";
import { ModalRefs } from "../../models/Modal";
import { BlogViewModal } from "../Modals";
import { StarIcon } from "@heroicons/react/outline";
import { AddFavorite, DeleteFavorite, GetSingleUserFavorite} from "../../api/favorites";

export const BlogDisplay = ({ blog, username }: { blog: Blog, username: String }) => {
  const fav = GetSingleUserFavorite(username, blog.blogId)
  let isFavorite = fav == null ?'text-white': 'text-yellow-300'
  const viewBlogModal = createRef<ModalRefs>();
  

  // useEffect(() => {
  //   console.log(fav)
  // }, [])

  const changeFavoriteState = () => {
   window.location.reload()  
   if (isFavorite === 'text-white'){
    isFavorite = 'text-yellow-300'
    AddFavorite(username, blog.blogId)
   }else{
    isFavorite = 'text-white'
    DeleteFavorite(username, blog.blogId)
   }
  }


  return (
    <div className="m-2 mx-[4%] text-left max-h-[200px]">
      <div className="flex flex-col">
        <div
          onClick={() => viewBlogModal.current?.openDialog()}
          className="flex flex-row cursor-pointer hover:bg-gray-900"
        >
          {<BlogViewModal blog={blog} ref={viewBlogModal} />}
          <div className="2xl:min-h-[160px] 2xl:min-w-[160px] mr-3 displaySmall:min-w-[125px] displaySmall:min-h-[125px] lg:min-w-[125px] lg:min-h-[125px]">
            <img
              className="object-cover 2xl:w-[160px] 2xl:h-[160px] displaySmall:w-[125px] displaySmall:h-[125px] lg:w-[125px] lg:h-[125px]"
              src={blog.image}
              alt="File not Found"
            />
          </div>

          <div>
            <div className="flex">
              <h1 className="overflow-hidden font-bold 2xl:text-2xl grow displaySmall:text-xl lg:text-xl text-ellipsis max-h-8">
                {blog.title}
              </h1>
              <h1 className="text-right grow 2xl:text-sm displaySmall:text-xs lg:text-xs">
                {String(blog.date_created).substring(0, 10)}
              </h1>
            </div>
            <div className="text-ellipsis overflow-hidden 2xl:max-h-[100px] displaySmall:max-h-[80px] lg:max-h-[82px]">
              <p className="2xl:text-base displaySmall:text-sm lg:text-sm">
                {blog.content}
              </p>
            </div>
          </div>
        </div>

        <div className="flex mt-2">
          <div className="flex gap-2 align-middle grow">
            <div className="flex w-5" title="Favorite">
              <StarIcon onClick={() => changeFavoriteState()} className={"cursor-pointer hover:text-yellow-300 " + isFavorite} />
            </div>
          </div>
          <p className="text-right grow 2xl:text-sm displaySmall:text-xs lg:text-xs">
            Author: {blog.userId}
          </p>
        </div>
      </div>
      <hr className="border-1 border-slalomBlue mt-3 rounded-full w-[100%] text-center" />
    </div>
  );
};
