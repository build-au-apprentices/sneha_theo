export interface ApiResponse {
  totalCount: number;
  results: Post[];
}

export interface Post {
  title: string;
  published: boolean;
  desc: string;
  id: number;
  created_at: string;
  published_at: string;
  user: User;
  comments: Comment[];
}


export interface Comment {
  commentId: number;
  blogId: number;
  userId: string;
  message: string;
  date_created: Date;
  date_mod ?: Date;
}

export interface Blog {
  blogId: number,
  title: string,
  userId: string,
  summary: string,
  content: string,
  isPublished: boolean,
  image: string,
  date_created?: Date,
  date_mod?: Date,
  likes: number
}

export interface User {
  userId: string       
  firstName: string
  lastName: string
  pronouns: string
  email: string       
  date_created: Date     
  date_mod: Date    
  isAdmin: boolean    
  profile_img:  string
}