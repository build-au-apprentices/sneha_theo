import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import "./index.css";
import Amplify from 'aws-amplify';

Amplify.configure({
  Auth: {
    // identityPoolId: 'arn:aws:cognito-idp:us-east-1:003333048540:userpool/us-east-1_XGh30mgIJ',
    region: 'ap-southeast-2',
    identityPoolRegion: 'ap-southeast-2',
    userPoolId: 'ap-southeast-2_DuqJJmWkU',
    userPoolWebClientId: '503eqosghkss2kb8lkd7382su0'
  }    
});


ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
