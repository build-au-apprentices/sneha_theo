
import CssBaseline from "@mui/material/CssBaseline";
import { Auth } from "aws-amplify";
import axios from "axios";
import { useEffect, useState } from "react";
import "./App.css";
import { Header } from "./components/Header";
import { RouteModel } from "./components/Routes";


// Blog posts: 
function App() {
  
  //Setting the header for what the user can see

  return (
    <div className="h-screen p-0 m-0">
      <CssBaseline />
        <Header/>

      <RouteModel />
    </div>
  );
}

export default App;
