import { Quote } from "../components/BlogDisplay";
import { createRef, useState } from "react";
import { Auth } from "aws-amplify";
import { ModalRefs } from "../models/Modal";
import { ForgotPasswordModal } from "../components/Modals/ForgotPasswordModal";

export const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorVal, setErrorVal] = useState('')

  const openForgetPasswordModalRef = createRef<ModalRefs>();

  const onSubmit = (event: any) => {
    event.preventDefault();

    
      const user = Auth.signIn(email, password).then(() => {
        //console.log(user);
        window.location.href = "/home";
      }).catch((error) => {
        setErrorVal('Incorrect username or password')
        console.log(error)
      })
  };

  return (
    <div className="flex flex-row h-screen text-white bg-black">
      <Quote></Quote>

      <div className="flex items-center justify-center w-2/3 h-[100%] displaySmall:w-full">
        <div className="sm:w-[90%] md:[80%] lg:w-[75%] xl:w-[55%]">
          <div className="bg-white shadow-lg rounded px-12  mb-4 pt-[150px] pb-[200px]">
          <form
            className=""
            onSubmit={onSubmit}
          >
            <div className="flex justify-center py-2 mb-4 text-2xl text-gray-800 border-b-2">
              Login To Your Account
            </div>
            <div className="mb-4">
              <label className="block mb-2 text-sm font-normal text-gray-700">
                Email
              </label>
              <input
                className="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                name="email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                v-model="form.email"
                type="text"
                required
                placeholder="Email"
              />
            </div>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-normal text-gray-700">
                Password
              </label>
              <input
                className="w-full px-3 py-2 mb-3 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                v-model="form.password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                type="password"
                placeholder="Password"
                name="password"
                required
              />
            </div>
            <div className="flex items-center justify-between">
              <div className="flex flex-row justify-between gap-2 item-center">
                <button
                  className="inline-block px-8 py-2 text-white rounded shadow-lg bg-slalomBlue hover:bg-sky-400"
                  type="submit"
                >
                  Sign In
                </button>
                <label className="text-sm text-red-600 ">{errorVal}</label>
              </div>
              
            </div>
          </form>
          <div className="flex flex-row justify-between mt-2 text-slalomBlue">
            <button onClick={() => openForgetPasswordModalRef.current?.openDialog()} className=" hover:text-sky-600">
              Forgot Password?
            </button>
            {<ForgotPasswordModal ref={openForgetPasswordModalRef} />}
            <a
              className="inline-block font-normal text-md hover:text-sky-600"
              href="/signup"
            >
              Sign up for an account
            </a>
            
          </div>
          </div>
        </div>
      </div>
    </div>
  );
};
