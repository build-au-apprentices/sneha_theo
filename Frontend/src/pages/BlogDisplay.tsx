import { useState } from 'react'
import LikedButtonActive from '../images/like-button-active.png'
import LikedButtonInactive from '../images/like-button-inactive.png'
import { Link } from "react-router-dom";


interface Blog {
    title: string,
    summary: string,
    text: string,
    isPublished: boolean,
    author: string,
    headerImage: string,
    dateCreated: Date,
    dateModified: Date,
    likes: number
}

export const BlogDisplay = ({ blog }: { blog: Blog }) => {
    const [likedState, setLiked] = useState(LikedButtonInactive);
    const [likesAmount, setLikesAmount] = useState(blog.likes)

    const changeLikedImage = () => {
        setLiked((likedState === LikedButtonInactive) ? LikedButtonActive : LikedButtonInactive)
        setLikesAmount((likedState === LikedButtonInactive) ? (likesAmount + 1) : (likesAmount - 1))
    }

    return (
        <div className='m-2 mx-[4%] text-left max-h-[200px]'>
            <div className='flex flex-col hover:bg-slate-900'>
                <div className='flex flex-row cursor-pointer'>
                    <div className='2xl:min-h-[150px] 2xl:min-w-[150px] mr-3 displaySmall:min-w-[125px] displaySmall:min-h-[125px] lg:min-w-[125px] lg:min-h-[125px]'>
                        <img className='object-cover 2xl:w-[500px] 2xl:h-[150px] displaySmall:w-[125px] displaySmall:h-[125px] lg:w-[125px] lg:h-[125px]' src={blog.headerImage} alt='File not Found' />
                    </div>

                    <div>
                    <Link className="blogLink" to="/blog"><div className='flex'>
                           <h1 className='2xl:text-2xl grow displaySmall:text-xl lg:text-xl font-bold text-ellipsis overflow-hidden max-h-8'>{blog.title}</h1>
                            <h1 className='grow text-right 2xl:text-sm displaySmall:text-xs lg:text-xs' >{blog.dateCreated.toDateString()}</h1>
                        </div>
                        <div className='text-ellipsis overflow-hidden 2xl:max-h-[100px] displaySmall:max-h-[80px] lg:max-h-[90px]'>
                            <p className='2xl:text-base displaySmall:text-sm lg:text-sm'>{blog.text}</p>
                        </div></Link>
                    </div>
                </div>

                <div className='flex mt-2'>
                    <div className='flex gap-2 align-middle grow'>
                        <img
                            onClick={changeLikedImage}
                            className='cursor-pointer 2xl:w-[15px] 2xl:h-[15px] mt-[3px] displaySmall:w-[10px] displaySmall:h-[10px] lg:w-[10px] lg:h-[10px]'
                            src={likedState}
                            alt='File not Found'
                        />
                        <p className='2xl:text-sm displaySmall:text-xs lg:text-xs' >{likesAmount}</p>
                    </div>
                    <p className='text-right grow 2xl:text-sm displaySmall:text-xs lg:text-xs' >Author: {blog.author}</p>
                </div>

            </div>
            <hr className='border-1 border-b-slate-400 mt-3 rounded-full w-[100%] text-center' />
        </div>
    )
}