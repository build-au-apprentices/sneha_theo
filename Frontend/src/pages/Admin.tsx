import React, { useEffect, useState } from "react";
import { AdminUserTabs } from "../components/AdminUserTabs";
//import { USERS } from "../components/DummyData";
import { User } from "../models";
import { LoadUsers } from "../api/user";

export const Admin = () => {
  const [search, setSearch] = useState("");
  //const [user, setUser] = useState([]);
  const user = LoadUsers();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value);
    console.log(search);
  };

  //useEffect(()=>{
    //setUser(LoadUsers());
    console.log(user);

  //},[])
  
  function getUsers() {
    return (
      <div className=" overflow-y-scroll h-[90%]">
        {user
          .filter((user: User) => user.email.includes(search))
          .map((user: User) => (
            <AdminUserTabs key={user.userId} user={user} />
          ))}
      </div>
    );
  }

  return (
    <div className="h-screen text-white bg-black">
      <div className="mx-[10%] h-screen">
        <div className="text-center">
          <h1>Search Users</h1>
          <input
            type="text"
            value={search}
            onChange={handleChange}
            placeholder="Search"
            className="mx-2 w-[65%] rounded-full h-4 px-2 text-black align-middle text-sm"
          />
        </div>
        <div className=" h-[90%]">
          <div className="flex flex-row justify-between mt-4 mb-1 text-center">
            <p className="w-[6.2857%] adminDisplay:w-[25%]">Selected</p>
            <p className="w-[18.2857%] adminDisplay:w-[25%]">First Name</p>
            <p className="w-[18.2857%] adminDisplay:w-[25%]">Last Name</p>
            <p className="w-[22.2857%] adminDisplay:hidden">Email</p>
            <p className="w-[8.2857%] adminDisplay:hidden">Pronouns</p>
            <p className="w-[20.2857%] adminDisplay:hidden">Date Created</p>
            <p className="w-[6.2857%] adminDisplay:w-[25%]">Admin</p>
          </div>
          <hr className="border-dashed "></hr>
          <div className=" overflow-y-scroll h-[90%]">{getUsers()}</div>
          <div className="flex justify-between my-3">
            <button className="text-white shadow-sm shadow-red-900 bg-red-800 hover:bg-red-600 rounded-xl p-1 w-[135px]">
              Delete Selected
            </button>
            <button className="p-1 text-white bg-gray-800 rounded-xl hover:border-2 hover:border-slalomBlue">
              New User
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
