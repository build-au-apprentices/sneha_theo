import { Quote } from "../components/BlogDisplay";
import { useState } from "react";
import { Auth } from "aws-amplify";
//import { UseCreateUser } from "../api/UserAPI";
import axios from "axios";

export const SignUp = () => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [name, setFirstName] = useState("");
  const [family_name, setlastName] = useState("");

  const onSubmit = (event: any) => {
    event.preventDefault();
    if (valPassword()) {
      try {
        const user = Auth.signUp({
          username,
          password,
          attributes: {
            email,
            name,
            family_name,
          },
        })
          .then(() => {
            console.log(user);
            createUser();
            window.location.href = "/";
          })
          .catch((error) => {
            var strError = String(error);
            if (strError.includes("Username cannot be of email format")) {
              setError("Username cannot be of email format");
              console.log("Username cannot be of email format");
            } else if (strError.includes("User already exists")) {
              setError("User already exists");
              console.log("User already exists");
            }

            //TODO duplicate user email
            console.log(error);
          });
      } catch (error) {
        console.log("error signing up:", error);
      }
    }
  };

  const createUser = () => {
    const user = {
      userId: username,
      firstName: name,
      lastName: family_name,
      email: email
    };

    axios.post("http://localhost:8080/createUser", user).then((response) => {
      console.log(response);
    });
  };

  const valPassword = () => {
    var retVal = true;

    if (password !== confirmPassword) {
      setError("Passwords do not match");
      retVal = false;
    }

    return retVal;
  };

  return (
    <div className="flex flex-row h-screen text-white bg-black">
      <Quote></Quote>

      <div className="flex items-right justify-end w-[65%] h-[100%]">
        <div className="  w-[70%]">
          <form
            className="bg-white shadow-lg  h-[100%] rounded px-12  mb-4 pt-[150px] pb-[200px]"
            onSubmit={onSubmit}
          >
            <div className=" items-center w-[75%] mx-auto">
              <div className="flex justify-center py-2 mb-4 text-2xl font-bold text-gray-800 border-b-2">
                Create An Account
              </div>
              <div className="mb-4">
                <input
                  className="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                  name="username"
                  value={username}
                  onChange={(event) => setUsername(event.target.value.toLowerCase())}
                  v-model="form.username"
                  type="text"
                  required
                  placeholder="Username"
                />
              </div>
              <div className="mb-4">
                <input
                  className="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                  name="email"
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
                  v-model="form.email"
                  type="email"
                  required
                  placeholder="Email"
                />
              </div>
              <div className="mb-4">
                <input
                  className="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                  name="firstName"
                  value={name}
                  onChange={(event) => setFirstName(event.target.value)}
                  v-model="form.firstName"
                  type="text"
                  required
                  placeholder="First Name"
                />
              </div>
              <div className="mb-4">
                <input
                  className="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                  name="lastName"
                  value={family_name}
                  onChange={(event) => setlastName(event.target.value)}
                  v-model="form.lastName"
                  type="text"
                  required
                  placeholder="Last Name"
                />
              </div>
              <div className="flex flex-col mb-6">
                <input
                  className="w-full px-3 py-2 mb-3 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                  v-model="form.password"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                  type="password"
                  placeholder="Password"
                  name="password"
                  required
                />

                <input
                  className="w-full px-3 py-2 mb-3 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                  v-model="form.password"
                  value={confirmPassword}
                  onChange={(event) => setConfirmPassword(event.target.value)}
                  type="password"
                  placeholder="Confirm Password"
                  name="password"
                  required
                />
                <label className="text-sm text-center text-red-500">
                  {error}
                </label>
              </div>
              <div className="flex items-center">
                <button
                  className="px-8 w-[100%] font-bold py-2 rounded text-white inline-block shadow-lg bg-slalomBlue hover:bg-blue-600 focus:bg-blue-700"
                  type="submit"
                >
                  Sign Up
                </button>
              </div>
              <div className="flex flex-row justify-between mt-3">
                <a
                  className="font-normal text-md text-slalomBlue hover:text-blue-800"
                  href="/"
                >
                  Already have an account? Login
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
