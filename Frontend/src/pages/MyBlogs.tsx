import { ProfileSideDisplay } from "../components/ProfileSideDisplay";
import { Blog, User } from "../models";
import { PencilAltIcon, StarIcon, DocumentTextIcon, CollectionIcon } from "@heroicons/react/outline";
import { createRef, useState } from "react";
import { ModalRefs } from "../models/Modal";
import { NewBlogModal } from "../components/Modals/NewBlogModal";
import { UserBlogTabs } from "../components/UserBlogTabs";
import { UserBlogs, BlogsById } from "../api/blogs";
import { Drafts } from "../api/blogs";
import { GetUser } from "../api/user";
import { GetUserFavorites } from "../api/favorites";

interface Fav {
  blog: Blog
}

export const Blogs = () => {
  const openNewBlogModalRef = createRef<ModalRefs>();
  const { drafts } = Drafts();
  const blogs  = UserBlogs();
  const [myBlogs, setMyBlogs] = useState(true);
  const [searchParams, setSearchParams] = useState('')
  const [fav, setFav] = useState(false);
  const [isDraft, setIsDraft] = useState(false);
  const favorite = GetUserFavorites();

  //const username = Authenticate();
  const user = GetUser();

  const handleDrafts = () =>{
    setIsDraft(true);
    setMyBlogs(false);
    setFav(false);
  };

  const handleFav = () =>{
    setIsDraft(false);
    setMyBlogs(false);
    setFav(true);
  };

  const handleMyBlogs = () =>{
    setIsDraft(false);
    setMyBlogs(true);
    setFav(false);
  };

  function blogTypes() {
    if (myBlogs) {
      return (
        blogs.filter((blog: Blog) => blog.title.toLowerCase().includes(searchParams.toLowerCase())).map((blog: Blog) => (
          <UserBlogTabs key={blog.blogId} blog={blog} />
        ))
        // "hello"
      )
    }
    else if(isDraft){
      return(
        drafts.filter((blog: Blog) => blog.title.toLowerCase().includes(searchParams.toLowerCase())).map((blog: Blog) => (
          <UserBlogTabs key={blog.blogId} blog={blog} isDraft={true} />
        ))
      )
    }
    else if(fav){
     
      
      return(
        favorite.map((fav:Fav)=> fav.blog ? <UserBlogTabs blog={fav.blog} key={fav.blog.blogId} isFav={true}/>:"")
        
      )
    }
  }
  function profile(){
    //console.log(user)
    return(
      user.map((user:User)=>(<ProfileSideDisplay key={user.userId} user={user}/>))
    )
    //<ProfileSideDisplay user={USERS[0]} />
    
  }

  return (
    <div className="bg-black h-[100%] w-full fixed flex flex-row text-white displaySmall:justify-center">
      <div className="w-[25%] displaySmall:hidden">
        {/* Profile Info */}
        {profile()}
      </div>
      <div className=" h-[85%] border-[1px] mb-28 border-white rounded-lg flex self-center displaySmall:hidden"></div>
      <div className="w-[75%] flex flex-col displaySmall:w-[100%]">
        <div className="flex flex-row justify-center my-2">
          {/* Search */}
          <input
            type="text"
            placeholder="Search"
            className="mx-2 w-[70%] rounded-full h-4 px-2 text-black align-middle text-sm"
            onChange={(e) => setSearchParams(e.target.value)}
            value={searchParams}
          />
          <select
            placeholder="Sort"
            className=" mx-2 w-[10%] rounded-full h-4 text-black text-[12px] align-middle text-center"
          >
            <option value="">Sort</option>
            <option value="Likes (High to Low)">Likes (High to Low)</option>
            <option value="Likes (Low to High)">Likes (Low to High)</option>
            <option value="Views (High to Low)">Views (High to Low)</option>
            <option value="Views (Low to High)">Views (Low to High)</option>
          </select>
        </div>
        <div className='h-[100%]'>
          {/* Blogs */}
          <div className="overflow-y-scroll h-[85%]">
            {blogTypes()}
          </div>
        </div>
      </div>
      <div className="flex flex-col w-[100px] justify-start mt-[5%] displaySmall:w-[65px]">
        <div className="flex flex-row justify-center">
          <div className="flex flex-col gap-2 w-[58px] displaySmall:w-[48px]">
            <div title="My Blogs">
              <CollectionIcon 
              onClick= {()=>handleMyBlogs()}
              className={"my-3 cursor-pointer hover:text-slalomBlue " + (myBlogs ? 'text-slalomBlue' : 'text-white')} 
              />
            </div>
            <div title="Drafts">
              <DocumentTextIcon 
              onClick= {()=>handleDrafts()}
              className={"my-3 cursor-pointer hover:text-slalomBlue " + (isDraft ? 'text-slalomBlue' : 'text-white')} 
              />
            </div>
            <div title="Favorites">
              <StarIcon 
              onClick= {()=>handleFav()}
              className={"my-3 cursor-pointer hover:text-slalomBlue " + (fav ? 'text-slalomBlue' : 'text-white')} />
            </div>
            <div title="New Blog">
              <PencilAltIcon
                onClick={() => openNewBlogModalRef.current?.openDialog()}
                className="my-3 cursor-pointer hover:text-slalomBlue"
              />
              {<NewBlogModal isNew={true} ref={openNewBlogModalRef} />}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
