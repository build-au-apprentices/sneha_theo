import { Blog } from "../models";
import { BlogDisplay } from "../components/BlogDisplay";
import { Quote } from "../components/BlogDisplay";
import { useEffect, useState } from "react";
import { Auth } from "aws-amplify";
import { useLoadBlogs } from "../api/blogs";
import { BlurOff } from "@mui/icons-material";

export const Home = () => {
  const [user, setUser] = useState("");
  const [status, setStatus] = useState(false);
  const [searchParams, setSearchParams] = useState('');
  const { blogs } = useLoadBlogs();

  useEffect(() => {
    async function onload() {
      try {
        const users = await Auth.currentAuthenticatedUser({
          bypassCache: true,
        });
        setUser(users.username);
        console.log(users);
      } catch (error) {
        alert(error);
      }
    }
    // const getallBlogs = async() => {
    //   try {
    //     const {data:response}= await axios.get("http://localhost:8080/allBlogs");
    //     setBlogs(response);
    //   } catch (error) {
    //     console.error(error);
    //   }

    // }

    onload();

    // getallBlogs();
  }, []);
  

  function getblog() {
    return (
      // <div className="overflow-y-scroll h-[90%] my-4">
      // {BLOGS.map((blog: Blog) => (
      //   <BlogDisplay blog={blog} />
      // ))}
      // </div>

      <div className="overflow-y-scroll h-[90%] my-4">
        {blogs.filter(
          (blog : Blog) => blog.title.toLowerCase().includes(searchParams.toLowerCase()) || blog.userId.includes(searchParams.toLowerCase()))
          .map((blog: Blog) => (
          <BlogDisplay key={blog.blogId} blog={blog} username={user}/>
        ))}
      </div>
    );
  }

  return (
    <div className="flex flex-row h-screen text-white bg-black">
      <Quote></Quote>
      <div className="h-[90%] flex-initial w-2/3 text-center displaySmall:w-full">
        <div>
          <input
            type="text"
            placeholder="Search"
            className="mx-2 w-[70%] rounded-full h-4 px-2 text-black align-middle text-sm"
            onChange={(e) => setSearchParams(e.target.value)}
            value={searchParams}
          />
          <select
            placeholder="Sort"
            className=" mx-2 w-[10%] rounded-full h-4 text-black text-[12px] align-middle text-center"
          >
            <option value="">Sort</option>
            <option value="Likes (High to Low)">Likes (High to Low)</option>
            <option value="Likes (Low to High)">Likes (Low to High)</option>
            <option value="Views (High to Low)">Views (High to Low)</option>
            <option value="Views (Low to High)">Views (Low to High)</option>
          </select>
        </div>
        {user !== "" ? getblog() : <div></div>}
      </div>
    </div>
  );
};
