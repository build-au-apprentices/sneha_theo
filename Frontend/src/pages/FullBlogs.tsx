import { useParams } from "react-router-dom";

interface Blog {
    title: string,
    blogid: number,
    summary: string,
    text: string,
    isPublished: boolean,
    author: string,
    headerImage: string,
    dateCreated: Date,
    dateModified: Date,
    likes: number
}

const BLOGS: Blog[] = [
    {
        title: 'The end of the world',
        blogid: 1,
        summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        isPublished: true,
        author: 'Theodore Polits',
        headerImage: '/images/city.jpg',
        dateCreated: new Date(),
        dateModified: new Date(),
        likes: 120
    },
    {
        title: 'Coffee spoils the new generation',
        blogid: 2,
        summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse. Varius vel pharetra vel turpis nunc eget lorem. Justo eget magna fermentum iaculis. Posuere lorem ipsum dolor sit amet.',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        isPublished: true,
        author: 'Theodore Polits',
        headerImage: '/images/coffee.jpg',
        dateCreated: new Date(),
        dateModified: new Date(),
        likes: 100
    },
    {
        title: 'In the world of a gamer',
        blogid: 3,
        summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse. Varius vel pharetra vel turpis nunc eget lorem. Justo eget magna fermentum iaculis. Posuere lorem ipsum dolor sit amet. Auctor neque vitae tempus quam pellentesque. Interdum consectetur libero id faucibus nisl tincidunt eget nullam. Ut etiam sit amet nisl purus in mollis nunc sed. Est placerat in egestas erat. Id diam vel quam elementum. Imperdiet nulla malesuada pellentesque elit eget gravida. Consequat nisl vel pretium lectus quam. Nunc sed augue lacus viverra vitae. Tellus rutrum tellus pellentesque eu. Montes nascetur ridiculus mus mauris vitae ultricies leo integer. ',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        isPublished: true,
        author: 'Theodore Polits',
        headerImage: '/images/game.jpg',
        dateCreated: new Date(),
        dateModified: new Date(),
        likes: 150
    },
    {
        title: 'Increase in car prices',
        blogid: 4,
        summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        isPublished: true,
        author: 'Theodore Polits',
        headerImage: '/images/car.jpeg',
        dateCreated: new Date(),
        dateModified: new Date(),
        likes: 105
    },
    {
        title: 'Beach Getaways',
        blogid: 5,
        summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.',
        isPublished: true,
        author: 'Theodore Polits',
        headerImage: '/images/beach.jpg',
        dateCreated: new Date(),
        dateModified: new Date(),
        likes: 67
    }
]


export const FullBlogs = () => {

    const  {id} = useParams();
 

    return (

        <div className="flex flex-row h-screen text-black justify-content-center">
            <div className="inline-block w-1/3 overflow-auto text-white bg-black profileSide displaySmall:hidden" >


                <h1 className="mt-[30px] ml-[20px] mb-[50px]  " style={{ fontSize: '30px', fontWeight: 'bold', textDecorationLine: 'underline' }}>Comments</h1>

                <div className='flex flex-row '>
                    <img className="w-[50px] h-[50px] object-fit rounded-full ml-2 " src="https://images.pexels.com/photos/2589653/pexels-photo-2589653.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" alt="Profile" />
                    <div className="ml-[15px]">
                        <div >
                            <h1 style={{ fontWeight: 'bold' }}>Theo Politis | 23 Februar 2022</h1>
                        </div>
                        <div className="w-[450px] mt-2">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>

                </div>
                <hr className='border-1 border-b-slate-400 mt-3 rounded-full w-[95%] text-center' />

                <div className='flex flex-row mt-3'>
                    <img className="w-[50px] h-[50px] object-fit rounded-full ml-2 " src="https://images.pexels.com/photos/2589653/pexels-photo-2589653.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" alt="Profile" />
                    <div className="ml-[15px]">
                        <div >
                            <h1 style={{ fontWeight: 'bold' }}>Theo Politis | 23 Februar 2022</h1>
                        </div>
                        <div className="w-[450px] mt-2">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt .</p>
                        </div>
                    </div>

                </div>

            </div>

            <div className='overflow-y-scroll w-2/3 h-[100%] ' >

                <div className='flex flex-row ml-[10%] mt-[30px]'>
                    <img className="w-[50px] h-[50px] object-fit rounded-full" src="https://images.pexels.com/photos/2589653/pexels-photo-2589653.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" alt="Profile" />
                    <div className="ml-[10px]">
                        <div >
                            <h1 style={{ fontWeight: 'bold' }}>Theo Politis</h1>
                        </div>
                        <div className="w-[350px] ">
                            <p>21/02/2022</p>
                        </div>
                    </div>

                </div>

                <h1 className="ml-[10%] my-[20px]" style={{fontSize:'35px', fontWeight:'bolder'}}> The End Of The World</h1>
                <img className="w-[80%] h-[30%] ml-[10%]" src="/images/city.jpg" alt='City'></img>
                <hr className='border-1 border-b-slate-400 mx-[10%] my-[30px] rounded-full w-[80%] text-center ' />
                <div className=" ml-[10%] mx-[10%]">
                <p >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.
                </p>

                <p >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit laoreet id. Gravida cum sociis natoque penatibus et magnis dis parturient. Duis ultricies lacus sed turpis tincidunt id aliquet. Porta non pulvinar neque laoreet suspendisse.
                </p>
                </div>


            </div>

        </div>
    )
};