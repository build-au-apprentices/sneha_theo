import axios from "axios";
import { useEffect, useState } from "react";
import { Authenticate } from "../components/Cognito/Authenticate";
import { Blog } from "../models";
import { BlogsById } from "./blogs";

interface Fav {
  blogId: Number
}
// API Call For favouring a blog
export const AddFavorite = (userId: String, blogId: Number) => {
  const request = async () => {
    const url = "http://localhost:8080/favorite/add";
    const data = {
      blogId,
      userId,
    };

    try {
      if (userId !== "") {
        const { data: response } = await axios.post(url, data);
        console.log(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  request();
};

// API Call for Favouring a blog
export const DeleteFavorite = (userId: String, blogId: Number) => {
  const request = async () => {
    const url = "http://localhost:8080/favorite/remove";
    try {
      if (userId !== "") {
        const { data: response } = await axios.delete(url, {
          data: { userId, blogId },
        });
        console.log(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  request();
};

// API call for retrieving a users favorite blogs
export const GetUserFavorites = () => {
  const [favorites, setFavorites] = useState([]);
  const userId = Authenticate();
  useEffect(() => {
    const request = async () => {
      const url = "http://localhost:8080/favorite/myFavorites/" + userId;
      try {
        if (userId !== "") {
          const { data: response } = await axios.get(url);
          setFavorites(response);
          //console.log(favorites);
        }
      } catch (error) {
        //console.log(error);
      }
    };

    request();
    
  }, [userId]);

  return favorites;
};

// Retrieve a single favorite query
export const GetSingleUserFavorite = (userId: String, blogId: number) => {
  
  const [favorite, setFavorite] = useState()
  useEffect(() => {
  const request = async () => {
    
    try {
      if (userId !== "" && blogId!==undefined) {
        const url = "http://localhost:8080/favorite/getFavorite/" + userId+ "/" + blogId;
        const { data: response } = await axios.get(url);
        setFavorite(response)

      }
    } catch (error) {
      console.log(error);
    }
  };

  request();
  }, [])

  return favorite;
};
