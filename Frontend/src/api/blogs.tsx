import axios from "axios";
import { useState, useEffect } from "react";
import { Authenticate } from "../components/Cognito";
import { Blog } from "../models";
//import { Authenticate } from "../components/Cognito/Authenticate"


export const DeleteBlog = (user:string, blogId:number) => {

  //const [blogs, setBlogs] = useState([]); 
   
  //useEffect(() => { 
    const deleteBlog = async() => {
      const url = "http://localhost:8080/blogs/delete/"+blogId;
     
    
      console.log("delete"); 
      //console.log(url);
      try {
        if(user!==""){
          const { data: response } = await axios.delete(url);
          console.log(response);
         }
       
      } catch (error) {
        console.error(error);
      }
    };

    deleteBlog();
  //},[user]);
};

export const Drafts = () => {

  const [drafts, setDrafts] = useState([]);
  const  user  = Authenticate();
   
  useEffect(() => { 
    const getDrafts = async() => {
      const url = "http://localhost:8080/blogs/drafts/" + user;
      //console.log(user); 
      //console.log(url);
      try {
        if(user!==""){
          const { data: response } = await axios.get(url);

          setDrafts(response);
        }
       
      } catch (error) {
        console.error(error);
      }
    };

    getDrafts();
  },[user]);
  return {
    drafts,
  };
};

export const CreateBlogs = (title:string,summary:string,content:string,image:string,likes:number, user:string, isPublished:boolean) => {

  //const [blogs, setBlogs] = useState([]); 
   
  //useEffect(() => { 
    const createBlogs = async() => {
      const url = "http://localhost:8080/blogs/create";
      const blog = {
        title,
        summary,
        userId: user,
        content,
        image,
        likes,
        isPublished
      }
    
      //console.log(url);
      try {
        if(user!==""){
          const { data: response } = await axios.post(url,blog);
          console.log(response);
         }
       
      } catch (error) {
        console.error(error);
      }
    };

    createBlogs();
  //},[user]);
};

export const useLoadBlogs = () => {

  const [blogs, setBlogs] = useState([]);  

  useEffect(() => {
    const getallBlogs = async () => {
      try {
        const { data: response } = await axios.get(
          "http://localhost:8080/blogs/published"
        );
        setBlogs(response);
      } catch (error) {
        console.error(error);
      }
    };
    getallBlogs();
  }, []);
  return {
    blogs,
  };
};

export const UpdateBlogs = (blogId:number,title:string,summary:string,content:string,image:string,likes:number, user:string, isPublished:boolean) => {

  //const [blogs, setBlogs] = useState([]); 
   
  //useEffect(() => { 
    const updateBlogs = async() => {
      const url = "http://localhost:8080/blogs/update/" + blogId;
      const blog = {
        title, 
        summary,
        userId: user,
        content,
        image,
        likes,
        isPublished
      }
      //console.log(url);
      try {
        if(user!==""){
          const { data: response } = await axios.put(url,blog);
          console.log(response);
         }
       
      } catch (error) {
        console.error(error);
      }
    };

    updateBlogs();
  //},[user]);
};

export const UserBlogs = () => {

  const [blogs, setBlogs] = useState([]);
  const user  = Authenticate();
   
  useEffect(() => { 
    const getallBlogs = async() => {
      const url = "http://localhost:8080/blogs/byUser/" + user;
      //console.log(user); 
      //console.log(url);
      try {
        if(user!==""){
          const { data: response } = await axios.get(url);

          setBlogs(response);
        }
       
      } catch (error) {
        console.error(error);
      }
    };

    getallBlogs();
  },[user]);
  return blogs
  
};

export const BlogsById = (blogId:Number) => {

  const [blogs, setBlogs] = useState({})
   
  //useEffect(() => { 
    const getBlogs = async() => {
      
      //console.log(user); 
      console.log("here");
      try {
        if(blogId){
          const url = "http://localhost:8080/blogs/byId/" + blogId;
          const { data: response } = await axios.get(url);
          
          setBlogs(response);
          console.log(blogs,"heree")
          
        }
       
      } catch (error) {
        console.error(error);
      }
    };

    getBlogs();
  //},[]);
 
  return blogs
  
};