import { useState, useEffect } from "react";
import axios from "axios";
import {User} from "../models"
import { Authenticate } from "../components/Cognito";



export const GetUser = (username?:string) => {
  
  //const [users, setUsers] = useState({});  
  const [users, setUsers] = useState([])
  const user = username ? username : Authenticate();

  useEffect(() => {
    const getallUsers = async () => {
      try {
        if (user !== "") {
          const url =  "http://localhost:8080/userByUsername/"+user;
          const { data: response } = await axios.get(url);
          setUsers(response); 
          
          return users;
        }
       
      } catch (error) {
        console.error(error);
      }
    };
    getallUsers();
  }, [user]);
 
  return  users ;
};


export const LoadUsers = () => {

  const [users, setUsers] = useState([]);  

  useEffect(() => {
    const getallUsers = async () => {
      try {
        const { data: response } = await axios.get(
          "http://localhost:8080/users"
        );
        setUsers(response);
        console.log(users)
      } catch (error) {
        console.error(error);
      }
    };
    getallUsers();
  }, []);
  return users
};

export const UpdateUser = (userId?:string,firstName?:string, lastName?:string, pronouns?:string, profile_img?:string, isAdmin?:boolean) => {

  //const [blogs, setBlogs] = useState([]); 
  //useEffect(() => { 
    const updateUser = async() => {
      const url = "http://localhost:8080/updateUser/" + userId;
      const user = {
        firstName, 
        lastName,
        pronouns,
        profile_img,
        isAdmin
      }
      //console.log(url);
      try {
        if(userId!==""){
          const { data: response } = await axios.put(url,user);
          console.log(response);
         }
       
      } catch (error) {
        console.error(error);
      }
    };

    updateUser();
  //},[userId]);
};