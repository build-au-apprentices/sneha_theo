export interface UserCreate{
    userId: string,
    firstName: string,
    lastName: string
    email: string
}