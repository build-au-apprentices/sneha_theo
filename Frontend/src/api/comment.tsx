import { useEffect, useState } from "react";
import axios from "axios";
//import { Authenticate } from "../components/Cognito/Authenticate"


export const CreateComment = (blogId: number, userId: string, message: string) => {

  //const [blogs, setBlogs] = useState([]); 

  //useEffect(() => { 
  const createComment = async () => {
    const url = "http://localhost:8080/comments/create";
    const comment = {
      blogId,
      userId,
      message
    }

    try {
      if (userId !== "") {
        const { data: response } = await axios.post(url, comment);
        console.log(response);
      }

    } catch (error) {
      console.error(error);
    }
  };

  createComment();
  //},[user]);
};

export const DeleteComment = (commentId: number) => {
  const deleteComment = async () => {
    const url = "http://localhost:8080/comments/delete/" + commentId;
    try {

      const { data: response } = await axios.delete(url);
      console.log(response);

    } catch (error) {
      console.error(error);
    }
  };

  deleteComment();
}

export const GetComments = (blogId:number) => {

  const [comments, setComments] = useState([]);  
  //let comments:[] = []; 
  useEffect(() => {
    const getallComments = async () => {
      try {
        const url = "http://localhost:8080/comments/allComments/" + blogId
        console.log(url)
        const { data: response } = await axios.get(url);
        setComments(response);
       
      } catch (error) {
        console.error(error);
      }
      
    };
    getallComments();
  }, []);
  //console.log(comments)
  return comments
};