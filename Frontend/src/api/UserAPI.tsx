import axios from "axios"
import { useState } from "react"
import { UserCreate } from "./Models/UserModels"



const BASE_POINT = 'http://localhost:8080'

export const UseCreateUser = async (user: UserCreate) => {
    await axios.post(BASE_POINT + '/createUser', user)
    .then((response) => {console.log('SUCCESS')})
    .catch((error) => {console.log('ERROR')})
}

export const GetUserByUsername = async (username : string) => {
    
    let res = await axios.get(BASE_POINT + '/userByUsername/' + username)
    .then((data) => console.log(data))
    .catch((error) => console.log(error))
}

export const GetAllUsers = async () => {
    const [users, setUsers] = useState([])

    const res = await axios.get(BASE_POINT + '/users');

    setUsers(res.data)
    return users
}