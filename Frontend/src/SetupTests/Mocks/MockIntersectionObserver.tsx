export const MockIntersectionObserver = () => ({
    observe: () => null,
    disconnect: () => null,
    unobserve: () => null,
})
