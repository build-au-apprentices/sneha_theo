# Apprentice Bootcamp

## Week 1: Frontend with React

Kodithuwakku, Dulshan
Soni, Kajal
Shi, Zoe
Sunil, Anne Maria

1. Split into pairs: [randomize teams](https://www.randomlists.com/team-generator?grp=5&items=Kodithuwakku%2C+Dulshan%0AChuah%2C+Yi+Jie%0APereira%2C+Varun%0APolitis%2C+Theo%0ADebsikdar%2C+Sneha%0ADrayton%2C+Henry%0AShi%2C+Zoe%0ABak%2C+Sothea-Roth%0ASoni%2C+Kajal%0ASunil%2C+Anne+Maria%0A)
   🧑‍🤝‍🧑
2. Walk through of [Figma designs](https://www.figma.com/file/QW0ssoqJC7DtXKZkeHI7Fv/Slalom_build-Accelerator) ✨
3. Let's create our [blog-starter-kit](https://bitbucket.org/slalom-consulting/apprentice_bootcamp_starter_kit/src/master/) 🔥
   - Start with [create-react-app](https://reactjs.org/docs/create-a-new-react-app.html)
   - Add routing with [react-router](https://reactrouter.com/)
   - Add components & styling with [react-material-ui](https://mui.com/)
   - Create some mock data using [Mockaroo](https://www.mockaroo.com/schemas/clone?clone=1645364382933)
   - Add state with useState/useContext (and other [hooks](https://reactjs.org/docs/hooks-intro.html))
4. Walk through of [opinionated-full-stack-python](https://bitbucket.org/slalom-consulting/opinionated-full-stack-python/src/main/) 📚
5. Unit Tests with [Jest](https://jestjs.io/) + [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/) 🐛
6. E2E with [Cypress](https://www.cypress.io/) 💕

### Additional sessions on React:

- [Session 1 (2h 25m)](https://twodegrees1-my.sharepoint.com/:v:/g/personal/nami_shah_slalom_com/EQmiuLye199FnIsjDTmu9EwB-HN2AVSCxXCcxjF9iZnY4Q?e=iYyaZu)
  Basic Javascript - equality checking, import/export syntax, common array/object functions, destructuring, other ES6 features
  Basic React: JSX, component hierarchy, passing props, intro to hooks and why they exist, Context API
- [Session 2 (1hr 40m)](https://twodegrees1-my.sharepoint.com/:v:/g/personal/nami_shah_slalom_com/Eci8x5XBG9tJuBUiHbN1_TkBmQbb2Vdh5c5iTQ-vCZNYhw?e=Xbv3yJ)
  Intermediate React - more hooks, how components render, application routing with React Router, fetching asynchronous data etc.
- [Session 3 (2h 48m)](https://twodegrees1-my.sharepoint.com/:v:/g/personal/janith_randeniya_slalom_com/EaBrVVwJTWRLkUNMb34wtxABiroS51rIAtFLEyA3GQ7-XA?e=9E1h7e)
  We built a calendar app from the ground up using React, Typescript, NextJS and TailwindCSS. A real world example of how to connect to an API, business logic around dates, styling for mobile responsiveness and much more.

## Week 2: Backwith with Express

Coming soon!

## Week 3: Cloud deployment into AWS

Coming soon!

## Week 3: Big Data

Coming soon!

## Week 4: Showcase!

Coming soon!
